from __future__ import print_function
import sys
from PySide2 import QtWidgets
import os

from FormRobotHandControl import FormRobotHandControl

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    print('path', os.environ['PATH'])
    print('LD_LIBRARY_PATH', os.environ.get('LD_LIBRARY_PATH'))
    w = FormRobotHandControl()
    w.show()

    sys.exit(app.exec_())

