from __future__ import print_function
import serial


class CommunicationError(Exception):
    pass

commands = {
    'full open': '0',
    'point': '1',
    'hook': '2',
    'tip': '3',
    'spherical': '4',
    'fingers': '9',
}

DEFAULT_DEVICE = '/dev/ttyACM0'
DEFAULT_BAUD_RATE = 9600
DEFAULT_TIMEOUT = 0.1


def send_robot_hand_control(command, device=DEFAULT_DEVICE,
                            baud_rate=DEFAULT_BAUD_RATE,
                            timeout=DEFAULT_TIMEOUT,
                            max_begin_attempts=None):
    print('open serial device', device)
    with serial.Serial(device, baud_rate, timeout=timeout) as arduino:
        if not arduino:
            raise CommunicationError()
        print('done')
        line = arduino.readline()
        attempts = 0
        while not line and (max_begin_attempts is None or attempts < max_begin_attempts):
            line = arduino.readline()
            attempts += 1
        print('first response', line)
        if isinstance(command, str):
            b = commands[command]
        else:
            b = bytes(''.join(commands[c] for c in command))
        print('sending bytes', b)
        arduino.write(bytes(b) + bytes('\n'))
        line = arduino.readline().strip()
    return line


if __name__ == '__main__':
    import argparse
    import sys
    parser = argparse.ArgumentParser()
    parser.add_argument('command', nargs='+')
    parser.add_argument('--device', default=DEFAULT_DEVICE)
    parser.add_argument('--baud-rate', type=int, default=DEFAULT_BAUD_RATE)
    parser.add_argument('--timeout', type=float, default=DEFAULT_TIMEOUT)
    parser.add_argument('--max-attempts', type=int, default=None)
    options = parser.parse_args()
    command = options.command
    device = options.device
    baud_rate = options.baud_rate
    timeout = options.timeout
    max_attempts = options.max_attempts
    print('send "{0}"'.format(command), 'to', device, 'at rate', baud_rate,
      'timeout', timeout, 'max_attempts', max_attempts)
    try:
        response = send_robot_hand_control(command, device, baud_rate, timeout, max_attempts)
        print('Response:', response)
    except CommunicationError:
        print('Failed to communicate with', device, file=sys.stderr)
        sys.exit(1)

