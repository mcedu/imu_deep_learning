"""
    Matthew Christian
    3d_grasp_training.py
    Train a deep-learning model on 3D IMU grasp configuration data
"""
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import tensorflow as tf

from my_models import *
from keras.utils.vis_utils import model_to_dot
from ann_visualizer.visualize import ann_viz
from PIL import Image
from time import time


NUM_CLASSES = 7
IMG_SHAPE = (28, 28, 3)


classes = {
    0: 'cylindrical',
    1: 'hook',
    2: 'pinch',
    3: 'point',
    4: 'spherical',
    5: 'tip',
    6: 'tripod',
}


def parse_record(filename):
    features = {
        'name': tf.FixedLenFeature((), tf.string),
        'image': tf.FixedLenFeature((), tf.string),
        'label': tf.FixedLenFeature((), tf.int64),
    }
    parsed = tf.parse_single_example(filename, features)
    name = tf.decode_raw(parsed['name'], tf.uint8)
    #name.set_shape(50)

    image = tf.decode_raw(parsed['image'], np.float32)
    image = tf.reshape(image, tf.stack(IMG_SHAPE))
    d = {
        'name': name,
        'CONV1_input': image,
    }
    label = tf.one_hot(parsed['label'], NUM_CLASSES)
    return d, label


def image_input_fn(filenames, shuffle=False, batchsize=20, repeat=True):
    dataset = tf.data.TFRecordDataset(filenames)
    dataset = dataset.map(lambda fn: parse_record(fn))
    if shuffle:
        dataset = dataset.shuffle(100)
    dataset = dataset.batch(batchsize, True)
    if repeat:
        dataset = dataset.repeat()
    iterator = dataset.make_initializable_iterator()
    return iterator


def plot_loss(model, history):
    plt.plot(history.history['loss'], 'o', label='Train')
    plt.plot(history.history['val_loss'], 'o', label='Validation')
    plt.title(model.name + ' Model Loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend()
    plot_fn = os.path.join('figures', '{0}_loss.png'.format(model.name))
    plt.savefig(plot_fn)
    plt.show()
    plt.close('all')


def main():
    models = [
        build_lenet_v3_model(IMG_SHAPE, NUM_CLASSES),
        #build_lenet_v4_model(IMG_SHAPE, NUM_CLASSES),
        #build_lenet_v5_model(IMG_SHAPE, NUM_CLASSES),
        #build_lenet_v6_model(IMG_SHAPE, NUM_CLASSES),
        #build_alexnet_model(IMG_SHAPE, NUM_CLASSES),
    ]
    train_files = tf.placeholder(tf.string, shape=[None])
    test_files = tf.placeholder(tf.string, shape=[None])
    val_files = tf.placeholder(tf.string, shape=[None])

    def train_and_test(sess, model):
        print('Training model', model.name)
        dataset = {
            'train': image_input_fn(train_files,
                                    shuffle=True,
                                    batchsize=50),
            'test': image_input_fn(test_files,
                                   shuffle=False,
                                   batchsize=20),
            'validation': image_input_fn(val_files,
                                         shuffle=True,
                                         batchsize=50,
                                         repeat=True),
        }
        sess.run(dataset['train'].initializer, feed_dict={
            train_files: ['tfrecords/train.tfrecord'],
        })
        sess.run(dataset['test'].initializer, feed_dict={
            test_files: ['tfrecords/test.tfrecord'],
        })
        sess.run(dataset['validation'].initializer, feed_dict={
            val_files: ['tfrecords/test.tfrecord'],
        })
        sess.run(tf.global_variables_initializer())
        starttime = time()
        history = model.fit(
            x=dataset['train'],
            epochs=14,
            steps_per_epoch=30,
            verbose=1,
            validation_data=dataset['validation'],
            validation_steps=1,
        )
        traintime = time() - starttime
        score = model.evaluate(
            x=dataset['test'],
            steps=7,
            verbose=1
        )
        print('Train time', traintime)
        train_time_fn = os.path.join('data', '{0}_train_time.txt'.format(model.name))
        with open(train_time_fn, 'w') as f:
            f.write('{0} seconds'.format(traintime))
        print('created', train_time_fn)
        test_data = {}
        for label, value in zip(model.metrics_names, score):
            test_data[label] = [value]
        print(test_data)
        df = pd.DataFrame.from_dict(test_data)
        test_fn = os.path.join('data', '{0}_test.csv'.format(model.name))
        df.to_csv(test_fn)
        print('created', test_fn)
        plot_loss(model, history)
        df = pd.DataFrame.from_dict(history.history)
        history_fn = os.path.join('data', '{0}_history.csv'.format(model.name))
        df.to_csv(history_fn)
        print('created', history_fn)

    def show_model(model):
        model_fn = os.path.join('visual', '{0}.png'.format(model.name))
        tf.keras.utils.plot_model(model, show_shapes=True, to_file=model_fn)
        print('created', model_fn)
        graphviz_fn = os.path.join('visual', '{0}.gv'.format(model.name))
        ann_viz(model, view=True, filename=graphviz_fn, title=model.name)
        print('created', graphviz_fn)

    def show_layer_outputs(model, filename):
        outdir = os.path.join('visual', model.name)
        os.makedirs(outdir, exist_ok=True)
        imgs = np.empty((IMG_SHAPE[0], IMG_SHAPE[1], 0))
        for plane in ('_XY', '_XZ', '_YZ'):
            img = Image.open(filename + plane + '.png').resize(IMG_SHAPE[:2], Image.ANTIALIAS).convert('L')
            #img.show()
            img_fn = os.path.join(outdir, 'input{0}.png'.format(plane))
            img.save(img_fn)
            img_arr = -1.0 * (np.array(img, dtype=np.float32) - 128) / 255.0
            img_arr = img_arr[:, :, np.newaxis]
            imgs = np.concatenate((imgs, img_arr), axis=-1)
        img_arr = imgs[np.newaxis, :, :, :]
        prev_phase = tf.keras.backend.learning_phase
        tf.keras.backend.set_learning_phase(0)  # Set testing phase
        for i, layer in enumerate(model.layers, 1):
            base_cls = str(layer.__class__.__bases__[0])
            if 'Conv' not in base_cls:
                continue
            #print('layer', layer, i)
            layer_dir = os.path.join(outdir, '{0}_{1}'.format(i, layer.name))
            os.makedirs(layer_dir, exist_ok=True)
            get_layer = tf.keras.backend.function(inputs=[model.layers[0].input],
                                                  outputs=[layer.output])
            out_arr = get_layer([img_arr])[0]
            out_arr = np.array((-1.0 * out_arr) * 255 + 128, dtype=np.uint8)
            #print('arr size:', out_arr.shape, out_arr.dtype)
            for j in range(out_arr.shape[-1]):
                feature_map = out_arr[0, :, :, j]
                #print('filter', j, 'shape:', feature_map.shape)
                fmap_img = Image.fromarray(feature_map, 'L')
                #print('output image size:', fmap_img.size)
                #fmap_img.show()
                w, h = fmap_img.size
                feature_fn = os.path.join(layer_dir, '{0:0>3}_{1}x{2}.png'.format(j + 1, w, h))
                fmap_img.save(feature_fn)
        if prev_phase in (0, 1):
            tf.keras.backend.set_learning_phase(prev_phase)

    def save_model(model):
        model_fn = os.path.join('model', '{0}.h5'.format(model.name))
        model.save(model_fn)

    def load_model(name):
        model_fn = os.path.join('model', '{0}.h5'.format(name))
        model = tf.keras.models.load_model(model_fn)
        return model

    def find_validation_losses(sess, model):
        val_dataset = image_input_fn(val_files,
                                     shuffle=False,
                                     batchsize=1,
                                     repeat=False)
        sess.run(val_dataset.initializer, feed_dict={
            val_files: ['tfrecords/test.tfrecord'],
        })
        sess.run(tf.global_variables_initializer())
        losses = pd.DataFrame(columns=['filename', 'label', 'loss'])
        i = 0
        loss_index = model.metrics_names.index('loss')
        try:
            next = val_dataset.get_next()
            while True:
                x, y = sess.run(next)
                score = model.evaluate(
                    x=x,
                    y=y,
                    verbose=0
                )
                #metrics = {label: value for label, value in zip(model.metrics_names, score)}
                name = x['name'].tostring().decode('utf-8').strip()
                label = classes[np.int(tf.argmax(y, axis=1).eval()[0])]
                loss = float(score[loss_index])
                losses = losses.append({'filename': name, 'label': label, 'loss': loss},
                                       ignore_index=True)
                i += 1
        except tf.errors.OutOfRangeError:
            pass
        losses.sort_values(by=['label', 'loss'], ascending=False, inplace=True)
        losses = losses.reset_index(drop=True)
        losses_fn = os.path.join('data', '{0}_losses.csv'.format(model.name))
        losses.to_csv(losses_fn)
        print('created', losses_fn)

    for model in models:
        sess = tf.Session()
        tf.keras.backend.set_session(sess)
        with sess.as_default():
            train_and_test(sess, model)
            save_model(model)
            # model = load_model(model.name)
            show_layer_outputs(model, 'imu_grasp_3d_dataset/test/spherical/010')
            find_validation_losses(sess, model)


if __name__ == '__main__':
    main()


