import numpy as np
import os
import pandas as pd
import random
import tensorflow as tf

from PIL import Image


datadir = 'imu_grasp_3d_dataset/'

groups = ['train', 'test', 'bad']

classes = {
    'cylindrical': 0,
    'hook': 1,
    'pinch': 2,
    'point': 3,
    'spherical': 4,
    'tip': 5,
    'tripod': 6,
}

column_names = [
    'ETime', 'Eh', 'Er', 'Ep',
    'Qx', 'Qy', 'Qz', 'Qw',
    'Gx', 'Gy', 'Gz',
    'Ax', 'Ay', 'Az',
    'LAx', 'LAy', 'LAz',
    'Mx', 'My', 'Mz',
    'Vx', 'Vy', 'Vz',
    'Px', 'Py', 'Pz',
]

img_size = (28, 28)


def create_example(imu_fn, label, group, group_dir):
    print(imu_fn)
    _, basename = os.path.split(imu_fn)
    basename, _ = os.path.splitext(basename)
    df = pd.read_csv(imu_fn, skiprows=1, delimiter='\s+', usecols=column_names)
    # Save all sensor data to csv
    csv_fn = os.path.join(group_dir, basename + '.csv')
    df.to_csv(csv_fn)
    # Open each image and convert to normalized single channel
    xy_fn = os.path.join(group_dir, basename + '_XY.png')
    xy_img = Image.open(xy_fn).resize(img_size, Image.ANTIALIAS).convert('L')
    xy_img = -1.0 * (np.array(xy_img, dtype=np.float32) - 128) / 255.0
    # xy_img = (128 - np.array(xy_img, dtype=np.float32)) / 255.0
    xz_fn = os.path.join(group_dir, basename + '_XZ.png')
    xz_img = Image.open(xz_fn).resize(img_size, Image.ANTIALIAS).convert('L')
    xz_img = -1.0 * (np.array(xz_img, dtype=np.float32) - 128) / 255.0
    # xz_img = (128 - np.array(xz_img, dtype=np.float32)) / 255.0
    yz_fn = os.path.join(group_dir, basename + '_YZ.png')
    yz_img = Image.open(yz_fn).resize(img_size, Image.ANTIALIAS).convert('L')
    yz_img = -1.0 * (np.array(yz_img, dtype=np.float32) - 128) / 255.0
    # yz_img = (128 - np.array(yz_img, dtype=np.float32)) / 255.0
    # big_img = np.concatenate((xy_img, xz_img, yz_img), axis=1)[:,:,np.newaxis]
    big_img = np.stack((xy_img, xz_img, yz_img), axis=2)
    # big_img.resize((224, 224, 1))
    name = "{0: <49}".format(imu_fn)[:49]
    example = tf.train.Example(
        features=tf.train.Features(feature={
            'name': tf.train.Feature(
                bytes_list=tf.train.BytesList(value=[name.encode()]),
            ),
            'image': tf.train.Feature(
                bytes_list=tf.train.BytesList(value=[big_img.tostring()])
            ),
            'label': tf.train.Feature(
                int64_list=tf.train.Int64List(value=[label])
            ),
        })
    )
    return example


def main():
    for group in groups:
        rec_fn = "tfrecords/{0}.tfrecord".format(group)
        examples = []
        with tf.python_io.TFRecordWriter(rec_fn) as writer:
            for cls, label in classes.items():
                group_dir = os.path.join(datadir, group, cls)
                if not os.path.exists(group_dir):
                    continue
                for fn in os.listdir(group_dir):
                    if not fn.endswith('.imu'):
                        continue
                    imu_fn = os.path.join(group_dir, fn)
                    examples.append(create_example(
                        imu_fn=imu_fn,
                        label=label,
                        group=group,
                        group_dir=group_dir
                    ))
            example_n = len(examples)
            # Randomly write the examples to the record
            random.shuffle(examples)
            while examples:
                example = examples.pop(0)
                writer.write(example.SerializeToString())
            print('wrote', example_n, 'examples to', rec_fn)


if __name__ == '__main__':
    main()

