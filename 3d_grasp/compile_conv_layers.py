from __future__ import print_function
from PIL import Image, ImageDraw
import argparse
import numpy as np
import os
import re
import sys

parser = argparse.ArgumentParser()
parser.add_argument('model_name')
parser.add_argument('--box-width', type=int, default=10)
parser.add_argument('--max-filters', type=int, default=10)
parser.add_argument('--draw-overlapping', action='store_true')
parser.add_argument('--draw-grid')
options = parser.parse_args()
model_name = options.model_name
box_width = options.box_width
max_filters = options.max_filters
draw_overlapping = options.draw_overlapping
draw_grid = [int(x) for x in options.draw_grid.split(',')]
if not draw_overlapping and not draw_grid:
    print('must draw overlapping or as grid', draw_overlapping, draw_grid)
    sys.exit(1)

model_path = os.path.join('visual', model_name)
for d in sorted(os.listdir(model_path)):
    m = re.search(r'((\d)_(\w+))', d)
    if not m:
        continue
    basename, ext = os.path.splitext(d)
    if ext:
        continue
    layer_dir = os.path.join(model_path, d)
    layer_name, layer_index, layer_subname = m.groups()
    filter_imgs = []
    filter_w = None
    filter_h = None
    for f in sorted(os.listdir(layer_dir)):
        m = re.search(r'(\d+)_(\d+)x(\d+).png', f)
        if not m:
            continue
        filter_index, w, h = m.groups()
        if not filter_w:
            filter_w = int(w)
            filter_h = int(h)
        filter_fn = os.path.join(layer_dir, f)
        filter_imgs.append(Image.open(filter_fn))
        if len(filter_imgs) >= max_filters:
            break
    n_filters = len(filter_imgs)
    draw_n_filters = min(n_filters, max_filters)
    if draw_overlapping:
        layer_img_w = filter_w + draw_n_filters * box_width
        layer_img_h = filter_h + draw_n_filters * box_width
        img = Image.new('RGBA', (layer_img_w, layer_img_h))
        draw = ImageDraw.Draw(img)
        draw.rectangle([0, 0, layer_img_w + 1, layer_img_h + 1], fill=(0, 0, 0, 0))
        for i, filter_img in enumerate(reversed(filter_imgs[:draw_n_filters])):
            img.paste(filter_img, ((draw_n_filters - i) * box_width,
                                   (draw_n_filters - i) * box_width))
    elif draw_grid:
        #grid_w, grid_h = draw_grid
        grid_w = int(np.ceil(np.sqrt(draw_n_filters)))
        grid_h = int(np.floor(np.sqrt(draw_n_filters)))
        layer_img_w = (filter_w + box_width) * grid_w
        layer_img_h = (filter_h + box_width) * grid_h
        img = Image.new('RGBA', (layer_img_w, layer_img_h))
        draw = ImageDraw.Draw(img)
        draw.rectangle([0, 0, layer_img_w + 1, layer_img_h + 1], fill=(0, 0, 0, 0))
        for i, filter_img in enumerate(filter_imgs[:draw_n_filters]):
            x = (i % grid_w) * (filter_w + box_width)
            y = (i // grid_w) * (filter_h + box_width)
            img.paste(filter_img, (x, y))
    layer_img_fn = os.path.join(model_path, layer_name + '.png')
    img.save(layer_img_fn)
    print('saved', layer_img_fn)

