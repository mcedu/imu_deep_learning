import numpy as np
import os
import pandas as pd
import re
import matplotlib.pyplot as plt
from scipy.ndimage.morphology import binary_erosion
import multiprocessing as mp

from PIL import Image


datadir = 'imu_grasp_3d_dataset/'

groups = ['train', 'test', 'bad']

classes = {
    'cylindrical': 0,
    'hook': 1,
    'pinch': 2,
    'point': 3,
    'spherical': 4,
    'tip': 5,
    'tripod': 6,
}

column_names = [
    'ETime', 'Eh', 'Er', 'Ep',
    'Qx', 'Qy', 'Qz', 'Qw',
    'Gx', 'Gy', 'Gz',
    'Ax', 'Ay', 'Az',
    'LAx', 'LAy', 'LAz',
    'Mx', 'My', 'Mz',
    'Vx', 'Vy', 'Vz',
    'Px', 'Py', 'Pz',
]

#img_size = (224, 224)


def generate_images(imu_fn, label, group, group_dir):
    print(imu_fn)
    _, basename = os.path.split(imu_fn)
    basename, _ = os.path.splitext(basename)
    df = pd.read_csv(imu_fn, skiprows=1, delimiter='\s+', usecols=column_names)
    # Get LA data
    pos = np.array(df[['Px', 'Py', 'Pz']], dtype=np.float32)
    # Save images
    Px, Py, Pz = pos[:, 0], pos[:, 1], pos[:, 2]
    #fig, ((axes['XY'], axes['YZ']), (axes['XZ'], _)) = plt.subplots(nrows=2, ncols=2)
    #_.axis('off')
    for i, plane in enumerate(('XY', 'XZ', 'YZ')):
        x_axis = pos[:, ord(plane[0]) - ord('X')]
        y_axis = pos[:, ord(plane[1]) - ord('X')]
        fig = plt.figure()
        ax = fig.gca()
        #ax = axes[plane]
        #ax.set_title(plane)
        #max_range = np.array([Px.max() - Px.min(), Py.max() - Py.min(), Pz.max() - Pz.min()]).max()
        max_range = np.array([1, 1, 1]).max()
        Xb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][0].flatten() + 0.5 * (Px.max() + Px.min())
        Yb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][1].flatten() + 0.5 * (Py.max() + Py.min())
        Zb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][2].flatten() + 0.5 * (Pz.max() + Pz.min())
        for xb, yb, zb in zip(Xb, Yb, Zb):
            ax.plot([xb], [yb], [zb], 'w')
        ax.plot(x_axis, y_axis, linewidth=10, color='black')
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        ax.axis('off')
        img_fn = os.path.join(group_dir, '{0}_{1}.png'.format(basename, plane))
        fig.savefig(img_fn)
        # plt.show()
        plt.close('all')

        #Erosion
        # img = Image.open(img_fn).convert('L')
        # img_arr = np.array(img, np.uint8)
        # masked = np.ma.array(img_arr, mask=binary_erosion(img_arr, iterations=30))
        # masked *= np.uint8(0)
        # img = Image.fromarray(img_arr)
        # img.save(img_fn)


def main():
    processes = []
    for group in groups:
        for cls, label in classes.items():
            group_dir = os.path.join(datadir, group, cls)
            if not os.path.exists(group_dir):
                continue
            for fn in sorted(os.listdir(group_dir)):
                if not fn.endswith('.imu'):
                    continue
                imu_fn = os.path.join(group_dir, fn)
                #p = mp.Process(target=generate_images, args=(imu_fn, label, group, group_dir))
                #processes.append(p)
                #p.start()
                generate_images(
                    imu_fn=imu_fn,
                    label=label,
                    group=group,
                    group_dir=group_dir,
                )
    # for p in processes:
    #     p.join()


if __name__ == '__main__':
    main()

