import tensorflow as tf
#from keras.layers.normalization import BatchNormalization


def build_lenet_v3_model(input_shape, num_classes):
    #Wide model v2
    model = tf.keras.models.Sequential(name="LeNet-5_v3")
    model.add(tf.keras.layers.Conv2D(name='CONV1',
                                     input_shape=input_shape,
                                     filters=6,
                                     kernel_size=(3, 3),
                                     strides=(1, 1),
                                     padding='same',
                                     activation='tanh'))
    model.add(tf.keras.layers.AveragePooling2D(name='AVGPOOL1',
                                               pool_size=(2, 2),
                                               strides=(1, 1),
                                               padding='valid'))
    model.add(tf.keras.layers.Conv2D(name='CONV2',
                                     filters=16,
                                     kernel_size=(3, 3),
                                     padding='valid',
                                     activation='tanh'))
    model.add(tf.keras.layers.AveragePooling2D(name='AVGPOOL2',
                                               pool_size=(2, 2),
                                               padding='valid'))
    model.add(tf.keras.layers.Conv2D(name='CONV3',
                                     filters=120,
                                     kernel_size=(3, 3),
                                     strides=(1, 1),
                                     padding='valid',
                                     activation='tanh'))
    model.add(tf.keras.layers.Flatten(name='FC1'))
    model.add(tf.keras.layers.Dense(name='FC2', units=1000, activation='tanh'))
    model.add(tf.keras.layers.Dense(name='FC3', units=num_classes,
                                    activation='softmax'))
    model.compile(loss=tf.keras.losses.categorical_crossentropy,
                  optimizer='SGD',
                  metrics=['categorical_accuracy', 'mae', 'mse'])
    return model


def build_lenet_v4_model(input_shape, num_classes):
    #Batch normalization model
    model = tf.keras.models.Sequential(name="LeNet-5_v4")
    model.add(tf.keras.layers.Conv2D(name='CONV1',
                                     input_shape=input_shape,
                                     filters=6,
                                     kernel_size=(3, 3),
                                     strides=(1, 1),
                                     padding='same',
                                     activation='tanh',))
    model.add(tf.keras.layers.AveragePooling2D(name='AVGPOOL1',
                                               pool_size=(2, 2),
                                               strides=(1, 1),
                                               padding='valid'))
    model.add(tf.keras.layers.Conv2D(name='CONV2',
                                     filters=16,
                                     kernel_size=(3, 3),
                                     padding='valid',
                                     activation='tanh'))
    model.add(tf.keras.layers.AveragePooling2D(name='AVGPOOL2',
                                               pool_size=(2, 2),
                                               strides=(2, 2),
                                               padding='valid'))
    model.add(tf.keras.layers.Conv2D(name='CONV3',
                                     filters=120,
                                     kernel_size=(3, 3),
                                     strides=(1, 1),
                                     padding='valid',
                                     activation='tanh'))
    model.add(tf.keras.layers.Flatten(name='FC1'))
    model.add(tf.keras.layers.Dense(name='FC2', units=1000))
    model.add(tf.keras.layers.BatchNormalization(name='BATCH1'))
    model.add(tf.keras.layers.Activation('tanh'))
    model.add(tf.keras.layers.Dense(name='FC3', units=num_classes,
                                    activation='softmax'))
    model.compile(loss=tf.keras.losses.categorical_crossentropy,
                  optimizer='SGD',
                  metrics=['categorical_accuracy', 'mae', 'mse'])
    return model


def build_lenet_v5_model(input_shape, num_classes):
    #Batch normalization with increased steps
    model = tf.keras.models.Sequential(name="LeNet-5_v5")
    model.add(tf.keras.layers.Conv2D(name='CONV1',
                                     input_shape=input_shape,
                                     filters=6,
                                     kernel_size=(3, 3),
                                     strides=(1, 1),
                                     padding='same',
                                     activation='tanh'))
    model.add(tf.keras.layers.AveragePooling2D(name='AVGPOOL1',
                                               pool_size=(2, 2),
                                               strides=(1, 1),
                                               padding='valid'))
    model.add(tf.keras.layers.Conv2D(name='CONV2',
                                     filters=16,
                                     kernel_size=(3, 3),
                                     padding='valid',
                                     activation='tanh'))
    model.add(tf.keras.layers.AveragePooling2D(name='AVGPOOL2',
                                               pool_size=(2, 2),
                                               strides=(2, 2),
                                               padding='valid'))
    model.add(tf.keras.layers.Conv2D(name='CONV3',
                                     filters=120,
                                     kernel_size=(3, 3),
                                     strides=(1, 1),
                                     padding='valid',
                                     activation='tanh'))
    model.add(tf.keras.layers.Flatten(name='FC1'))
    model.add(tf.keras.layers.Dense(name='FC2', units=1000))
    model.add(tf.keras.layers.BatchNormalization(name='BATCH1'))
    model.add(tf.keras.layers.Activation('tanh'))
    model.add(tf.keras.layers.Dense(name='FC3', units=num_classes,
                                    activation='softmax'))
    model.compile(loss=tf.keras.losses.categorical_crossentropy,
                  optimizer='SGD',
                  metrics=['categorical_accuracy', 'mae', 'mse'])
    return model


def build_lenet_v6_model(input_shape, num_classes):
    #Kernel Regularizer version
    model = tf.keras.models.Sequential(name="LeNet-5_v6")
    model.add(tf.keras.layers.Conv2D(name='CONV1',
                                     input_shape=input_shape,
                                     filters=6,
                                     kernel_size=(3, 3),
                                     strides=(1, 1),
                                     padding='same',
                                     activation='tanh',
                                     kernel_regularizer=tf.keras.regularizers.l2(0.)))
    model.add(tf.keras.layers.AveragePooling2D(name='AVGPOOL1',
                                               pool_size=(2, 2),
                                               strides=(1, 1),
                                               padding='valid'))

    model.add(tf.keras.layers.Conv2D(name='CONV2',
                                     filters=16,
                                     kernel_size=(3, 3),
                                     padding='valid',
                                     activation='tanh',
                                     kernel_regularizer=tf.keras.regularizers.l2(0.)))
    model.add(tf.keras.layers.AveragePooling2D(name='AVGPOOL2',
                                               pool_size=(2, 2),
                                               strides=(2, 2),
                                               padding='valid'))
    model.add(tf.keras.layers.Conv2D(name='CONV3',
                                     filters=120,
                                     kernel_size=(3, 3),
                                     strides=(1, 1),
                                     padding='valid',
                                     activation='tanh',
                                     kernel_regularizer=tf.keras.regularizers.l2(0.)))
    model.add(tf.keras.layers.Flatten(name='FC1'))
    model.add(tf.keras.layers.Dense(name='FC2', units=1000))
    model.add(tf.keras.layers.BatchNormalization(name='BATCH1'))
    model.add(tf.keras.layers.Activation('tanh'))
    #model.add(tf.keras.layers.Dropout(name='DROPOUT1', rate=0.5))
    model.add(tf.keras.layers.Dense(name='FC3', units=num_classes,
                                    activation='softmax'))
    model.compile(loss=tf.keras.losses.categorical_crossentropy,
                  optimizer='SGD',
                  metrics=['categorical_accuracy', 'mae', 'mse'])
    return model


def build_alexnet_model(input_shape, num_classes):
    model = tf.keras.models.Sequential(name='AlexNet')
    factor = 1

    # Layer 1
    model.add(tf.keras.layers.Conv2D(name='CONV1',
                                     input_shape=input_shape,
                                     filters=96//factor,
                                     kernel_size=(11, 11),
                                     padding='same',
                                     kernel_regularizer=tf.keras.regularizers.l2(0.)))
    model.add(tf.keras.layers.BatchNormalization(name='BATCH1'))
    model.add(tf.keras.layers.Activation('relu'))
    model.add(tf.keras.layers.MaxPooling2D(name='MAXPOOL1',
                                           pool_size=(2, 2)))

    # Layer 2
    model.add(tf.keras.layers.Conv2D(name='CONV2',
                                     filters=256//factor,
                                     kernel_size=(5, 5),
                                     padding='same'))
    model.add(tf.keras.layers.BatchNormalization(name='BATCH2'))
    model.add(tf.keras.layers.Activation('relu'))
    model.add(tf.keras.layers.MaxPooling2D(name='MAXPOOL2',
                                           pool_size=(2, 2)))

    # Layer 3
    model.add(tf.keras.layers.ZeroPadding2D((1, 1)))
    model.add(tf.keras.layers.Conv2D(name='CONV3',
                                     filters=512//factor,
                                     kernel_size=(3, 3),
                                     padding='same'))
    model.add(tf.keras.layers.BatchNormalization(name='BATCH3'))
    model.add(tf.keras.layers.Activation('relu'))
    model.add(tf.keras.layers.MaxPooling2D(name='MAXPOOL3',
                                           pool_size=(2, 2)))

    # Layer 4
    model.add(tf.keras.layers.ZeroPadding2D((1, 1)))
    model.add(tf.keras.layers.Conv2D(name='CONV4',
                                     filters=1024//factor,
                                     kernel_size=(3, 3),
                                     padding='same'))
    model.add(tf.keras.layers.BatchNormalization(name='BATCH4'))
    model.add(tf.keras.layers.Activation('relu'))

    # Layer 5
    model.add(tf.keras.layers.ZeroPadding2D((1, 1)))
    model.add(tf.keras.layers.Conv2D(name='CONV5',
                                     filters=1024//factor,
                                     kernel_size=(3, 3),
                                     padding='same'))
    model.add(tf.keras.layers.BatchNormalization(name='BATCH5'))
    model.add(tf.keras.layers.Activation('relu'))
    model.add(tf.keras.layers.MaxPooling2D(name='MAXPOOL5',
                                           pool_size=(2, 2)))

    # Layer 6
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(name='FC1', units=3072//factor))
    model.add(tf.keras.layers.BatchNormalization(name='BATCH6'))
    model.add(tf.keras.layers.Activation('relu'))
    model.add(tf.keras.layers.Dropout(name='DROPOUT1', rate=0.5))

    # Layer 7
    model.add(tf.keras.layers.Dense(name='FC2', units=4096//factor))
    model.add(tf.keras.layers.BatchNormalization(name='BATCH7'))
    model.add(tf.keras.layers.Activation('relu'))
    model.add(tf.keras.layers.Dropout(name='DROPOUT2', rate=0.5))

    # Layer 8
    model.add(tf.keras.layers.Dense(name='FC3', units=num_classes))
    model.add(tf.keras.layers.BatchNormalization(name='BATCH8'))
    model.add(tf.keras.layers.Activation('softmax'))

    model.compile(loss=tf.keras.losses.categorical_crossentropy,
                  optimizer='SGD',
                  metrics=['categorical_accuracy', 'mae', 'mse'])

    return model

"""
import tensorflow as tf

def build_position_projection_lenet_model(input_shape):
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Conv2D(name='CONV1',
                                     filters=6,
                                     kernel_size=(5, 5),
                                     strides=(1, 1),
                                     padding='same',
                                     activation='tanh',
                                     input_shape=input_shape))
    model.add(tf.keras.layers.AveragePooling2D(name='AVGPOOL1',
                                               pool_size=(2, 2),
                                               strides=(1, 1),
                                               padding='valid'))
    model.add(tf.keras.layers.Conv2D(name='CONV2',
                                     filters=16,
                                     kernel_size=(5, 5),
                                     strides=(1, 1),
                                     padding='valid',
                                     activation='tanh'))
    model.add(tf.keras.layers.AveragePooling2D(name='AVGPOOL2',
                                               pool_size=(2, 2),
                                               strides=(2, 2),
                                               padding='valid'))
    model.add(tf.keras.layers.Conv2D(name='CONV3',
                                     filters=120,
                                     kernel_size=(5, 5),
                                     strides=(1, 1),
                                     padding='valid',
                                     activation='tanh'))
    model.add(tf.keras.layers.Flatten(name='FC1'))
    model.add(tf.keras.layers.Dense(name='FC2', units=84, activation='tanh'))
    model.add(tf.keras.layers.Dense(name='FC3', units=NUM_CLASSES,
                                    activation='softmax'))
    model.compile(loss=tf.keras.losses.categorical_crossentropy,
                  optimizer='SGD',
                  metrics=['accuracy'])

    return model
"""

"""
def build_position_projection_vgg16_model(input_shape, num_classes):
    input_tensor = tf.keras.layers.Input(shape=input_shape, name='VGG16_input')
    #model = tf.keras.applications.vgg16.VGG16(include_top=True,
    #                                          weights=None,
    #                                          input_tensor=input_tensor,
    #                                          input_shape=None,
    #                                          pooling='avg',
    #                                          classes=num_classes)
    base_model = tf.keras.applications.vgg16.VGG16(include_top=False,
                                              weights='imagenet',
                                              input_tensor=input_tensor,
                                              input_shape=None,
                                              pooling='avg',
                                              classes=num_classes)
    x = base_model.output
    x = tf.keras.layers.Flatten()(x)
    #x = tf.keras.layers.GlobalAveragePooling1D()(x)
    x = tf.keras.layers.Dense(num_classes, activation='softmax')(x)
    model = tf.keras.Model(base_model.input, x)
    for layer in base_model.layers:
        layer.trainable = False
    model.compile(loss=tf.keras.losses.categorical_crossentropy,
                  optimizer='SGD',
                  metrics=['accuracy'])

    return model
"""
