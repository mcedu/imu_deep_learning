from __future__ import print_function
import argparse
import matplotlib.pyplot as plt
import os
import numpy as np
import re


def takeIntegral(f, t):
    integF = np.zeros(len(t))
    s = ((f[0] + 0) / 2.0) * t[0]
    integF[0] = s

    i = 1
    while i < len(t):
        dt = (t[i] - t[i - 1])
        s += ((f[i] + f[i - 1]) / 2.0) * dt
        integF[i] = s
        i += 1

    return integF


class IMUDataHelper:
    def __init__(self):
        pass

    def readFileContent(self, fileName):
        content = None
        with open(fileName) as f:
            content = f.read().splitlines()

        if content is None:
            return None

        if len(content) < 2:
            return None

        sampleCount, recordTime, laDriftVector = self._parseHeader(content[0])

        fields = self._seperateIntoList(content[1])
        fieldCount = len(fields)

        measurementData = np.zeros((sampleCount, fieldCount), dtype=float)

        measurementLineIndex = 2
        measurementLastLineIndex = sampleCount + measurementLineIndex
        while measurementLineIndex < measurementLastLineIndex:

            measurementFields = self._seperateIntoList(content[measurementLineIndex])

            measurementContainerPos = measurementLineIndex - 2

            for fieldIndex, measureStr in enumerate(measurementFields):
                measurementData[measurementContainerPos, fieldIndex] = float(measureStr)

            measurementLineIndex += 1

        return sampleCount, recordTime, laDriftVector, measurementData

    def _seperateIntoList(self, line):
        spaceCleanedLine = re.sub('\s\s+', ' ', line).strip()
        return spaceCleanedLine.split(' ')

    def _parseHeader(self, header):
        headerFiels = header.split(' ')
        if len(headerFiels) != 3:
            return None

        sampleCountField = headerFiels[0]
        timeStampField = headerFiels[1]
        laDriftField = headerFiels[2]

        sampleCountData = sampleCountField.split(':')
        timeStampData = timeStampField.split(':')
        laDriftData = laDriftField.split(':')

        if len(sampleCountData) != 2 or len(timeStampData) != 2 or len(laDriftData) != 2:
            return None

        sampleCount = int(sampleCountData[1])
        recordTime = long(timeStampData[1])
        laDriftStr = laDriftData[1]
        laDriftStr = laDriftStr.replace('[', '')
        laDriftStr = laDriftStr.replace(']', '')
        laDriftStrVector = laDriftStr.split(',')

        laDriftVector = [float(laDriftStrVector[0]), float(laDriftStrVector[1]), float(laDriftStrVector[2])]

        return sampleCount, recordTime, laDriftVector

    def create2DPlotFile(self, xData, yData, fileName):
        fig = plt.figure()
        ax = fig.gca()
        ax.plot(xData, yData, linewidth = 20, color = 'black')
        fig.savefig(fileName + '.png')
        plt.close('all')

    def save2DFigure(self, Px, Py, Pz, baseFileName):
        # Create cubic bounding box to simulate equal aspect ratio
        max_range = np.array([Px.max() - Px.min(), Py.max() - Py.min(), Pz.max() - Pz.min()]).max()
        Xb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][0].flatten() + 0.5 * (Px.max() + Px.min())
        Yb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][1].flatten() + 0.5 * (Py.max() + Py.min())
        Zb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][2].flatten() + 0.5 * (Pz.max() + Pz.min())
        # Comment or uncomment following both lines to test the fake bounding box:
        fig = plt.figure()
        ax = fig.gca()
        ax.plot(Px, Py, linewidth=30, color='black')

        for xb, yb, zb in zip(Xb, Yb, Zb):
            ax.plot([xb], [yb], [zb], 'w')
            fig.savefig(baseFileName + '_XY.png')
            plt.close('all')

            fig = plt.figure()
            ax = fig.gca()
            ax.plot(Px, Pz, linewidth=30, color='black')

        for xb, yb, zb in zip(Xb, Yb, Zb):
            ax.plot([xb], [yb], [zb], 'w')

            fig.savefig(baseFileName + '_XZ.png')
            plt.close('all')

            fig = plt.figure()
            ax = fig.gca()
            ax.plot(Py, Pz, linewidth=30, color='black')

        for xb, yb, zb in zip(Xb, Yb, Zb):
            ax.plot([xb], [yb], [zb], 'w')

            fig.savefig(baseFileName + '_YZ.png')
            plt.close('all')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('imu_file', default='test.imu')
    options = parser.parse_args()
    fileName = options.imu_file
    imuDataHelper = IMUDataHelper()
    sampleCount, recordTime, laDriftVector, measurementData = imuDataHelper.readFileContent(fileName)

    tIndex = 0
    LAxIndex = 14
    LAyIndex = 15
    LAzIndex = 16

    #imuDataHelper.create2DPlotFile(measurementData[:, tIndex]/1000.0, measurementData[:,LAxIndex],fileName)

    t = measurementData[:, tIndex] / 1000.0  # convert seconds
    LAx_m = measurementData[:, LAxIndex]
    LAy_m = measurementData[:, LAyIndex]
    LAz_m = measurementData[:, LAzIndex]
    LAxDrift, LAyDrift, LAzDrift = laDriftVector
    LAx = LAx_m - LAxDrift
    LAy = LAy_m - LAyDrift
    LAz = LAz_m - LAzDrift
    Vx = takeIntegral(LAx, t)
    Vy = takeIntegral(LAy, t)
    Vz = takeIntegral(LAz, t)
    Px = takeIntegral(Vx, t)
    Py = takeIntegral(Vy, t)
    Pz = takeIntegral(Vz, t)
    baseName, _ = os.path.splitext(fileName)
    imuDataHelper.save2DFigure(Px, Py, Pz, baseName)

    print(sampleCount, recordTime, laDriftVector)
    print(measurementData)
