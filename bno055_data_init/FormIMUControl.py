import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D
import numpy as np
import time
from PySide2 import QtWidgets
from datetime import datetime

from PySide2.QtCore import QFile, QObject, QTimer
from PySide2.QtUiTools import QUiLoader

# from Adafruit_BNO055 import BNO055
from BNO055_UPDATE import BNO055
from scipy.signal import butter, lfilter, freqz


# def butter_bandpass(lowcut, highcut, fs, order=5):
#     nyq = 0.5 * fs
#     low = lowcut / nyq
#     high = highcut / nyq
#     b, a = butter(order, [low, high], btype='band')
#     return b, a
#
#
# def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
#     b, a = butter_bandpass(lowcut, highcut, fs, order=order)
#     y = lfilter(b, a, data)
#     return y
#
#
#
# def butter_lowpass(cutoff, fs, order=5):
#     nyq = 0.5 * fs
#     normal_cutoff = cutoff / nyq
#     b, a = butter(order, normal_cutoff, btype='low', analog=False)
#     return b, a
#
# def butter_lowpass_filter(data, cutoff, fs, order=5):
#     b, a = butter_lowpass(cutoff, fs, order=order)
#     y = lfilter(b, a, data)
#     return y



class FormIMUControl(QObject):
    def __init__(self, parent=None):
        super(FormIMUControl, self).__init__()
        self.loadUiFile()

        self.bno = None
        self.connected = False

        self.timerCalib = QTimer(self)
        self.timerCalib.setInterval(1000)
        self.timerCalib.timeout.connect(self.on_calibTimer_timeout)

        self.timerIMUData = QTimer(self)
        self.timerIMUData.setInterval(1)
        self.timerIMUData.timeout.connect(self.on_timerIMUData_timeout)

        self.beginMeasurementTime = 0
        self.prevMeasurementTime = 0
        self.collectedData = []
        self.zeroGyroData = []
        self.dataCollectionStarted = False

        self.tIndex = 0
        self.LAxIndex = 14
        self.LAyIndex = 15
        self.LAzIndex = 16

        self.LAxDrift = 0.0
        self.LAyDrift = 0.0
        self.LAzDrift = 0.0

    def loadUiFile(self):
        ui_file = QFile('form_imu_control.ui')
        ui_file.open(QFile.ReadOnly)
        loader = QUiLoader()
        self.window = loader.load(ui_file)
        ui_file.close()

        self.lblConnection = self.window.findChild(QtWidgets.QLabel, 'lblConnection')
        self.btnConnect = self.window.findChild(QtWidgets.QPushButton, 'btnConnect')
        self.btnConnect.clicked.connect(self.on_btnConnect_click)

        self.btnDisconnect = self.window.findChild(QtWidgets.QPushButton, 'btnDisconnect')
        self.btnDisconnect.clicked.connect(self.on_btnDisconnect_click)

        self.tbCalibSytem = self.window.findChild(QtWidgets.QLineEdit, 'tbCalibSystem')
        self.tbCalibGyro = self.window.findChild(QtWidgets.QLineEdit, 'tbCalibGyro')
        self.tbCalibAcc = self.window.findChild(QtWidgets.QLineEdit, 'tbCalibAcc')
        self.tbCalibMag = self.window.findChild(QtWidgets.QLineEdit, 'tbCalibMag')

        self.btnCalibStart = self.window.findChild(QtWidgets.QPushButton, 'btnCalibStart')
        self.btnCalibStart.clicked.connect(self.on_btnCalibStart_click)

        self.btnCalibStop = self.window.findChild(QtWidgets.QPushButton, 'btnCalibStop')
        self.btnCalibStop.clicked.connect(self.on_btnCalibStop_click)

        self.btnCalibSave = self.window.findChild(QtWidgets.QPushButton, 'btnCalibSave')
        self.btnCalibSave.clicked.connect(self.on_btnCalibSave_click)

        self.btnCalibRestore = self.window.findChild(QtWidgets.QPushButton, 'btnCalibRestore')
        self.btnCalibRestore.clicked.connect(self.on_btnCalibRestore_click)

        self.btnCalculateLinAccDrift = self.window.findChild(QtWidgets.QPushButton, 'btnCalculateLinAccDrift')
        self.btnCalculateLinAccDrift.clicked.connect(self.on_btnCalculateLinAccDrift_click)

        self.tbEulerAngles = self.window.findChild(QtWidgets.QLineEdit, 'tbEulerAngles')
        self.tbQuaterion = self.window.findChild(QtWidgets.QLineEdit, 'tbQuaterion')
        self.tbGyroscope = self.window.findChild(QtWidgets.QLineEdit, 'tbGyroscope')
        self.tbAccelerometer = self.window.findChild(QtWidgets.QLineEdit, 'tbAccelerometer')
        self.tbLinearAccel = self.window.findChild(QtWidgets.QLineEdit, 'tbLinearAccel')
        self.tbMagnetometer = self.window.findChild(QtWidgets.QLineEdit, 'tbMagnetometer')

        self.btnIMUDataStartStop = self.window.findChild(QtWidgets.QPushButton, 'btnIMUDataStartStop')
        self.btnIMUDataStartStop.clicked.connect(self.on_btnIMUDataStartStop_clicked)

        self.btnSaveIMUData = self.window.findChild(QtWidgets.QPushButton, 'btnSaveIMUData')
        self.btnSaveIMUData.clicked.connect(self.on_btnSaveIMUData_clicked)

    def show(self):
        self.window.show()

    def on_btnConnect_click(self):
        self.bno = BNO055(serial_port='/dev/ttyUSB0', rst=None)
        self.connected = self.bno.begin()


        if self.connected:
            self.lblConnection.setText('ON')
        else:
            self.lblConnection.setText('OFF')

    def on_btnDisconnect_click(self):
        self.bno = None
        self.connected = False

        self.lblConnection.setText('OFF')
        self.timerCalib.stop()

    def on_calibTimer_timeout(self):
        sys, gyro, accel, mag = self.bno.get_calibration_status()
        self.tbCalibSytem.setText(str(sys))
        self.tbCalibGyro.setText(str(gyro))
        self.tbCalibAcc.setText(str(accel))
        self.tbCalibMag.setText(str(mag))

    def on_btnCalibStart_click(self):
        self.timerCalib.start()

    def on_btnCalibStop_click(self):
        self.timerCalib.stop()

    def on_btnCalibSave_click(self):
        calib_data = self.bno.get_calibration()

        folderName = 'calibration/'
        fileName = folderName + 'calibration_data'
        with open(fileName, 'w') as output:
            for item in calib_data:
                output.write(str(item) + ' ')

        #print calib_data

    def on_btnCalibRestore_click(self):

        folderName = 'calibration/'
        fileName = folderName + 'calibration_data'

        calib_data = []
        with open(fileName, 'r') as inputFile:
            dataLine = inputFile.readline()
            dataAsStrList = dataLine.split(' ')

            for dStr in dataAsStrList:
                try:
                    calib_data.append(int(dStr))
                except:
                    pass

            self.bno.set_calibration(calib_data)




    def _updateLinAccDrift(self):
        npData = np.array(self.zeroGyroData)
        LAx = npData[:, self.LAxIndex]
        LAy = npData[:, self.LAyIndex]
        LAz = npData[:, self.LAzIndex]

        self.LAxDrift = np.average(LAx)
        self.LAyDrift = np.average(LAy)
        self.LAzDrift = np.average(LAz)

    def on_btnCalculateLinAccDrift_click(self):
        self._updateLinAccDrift()



    def on_btnIMUDataStartStop_clicked(self):
        if not self.dataCollectionStarted:
            self.beginMeasurementTime = int(time.time() * 1000)
            self.prevMeasurementTime = self.beginMeasurementTime

            self.timerIMUData.start()
            self.collectedData = []
            self.zeroGyroData = []
            self.dataCollectionStarted = True
            self.btnIMUDataStartStop.setText('Stop')

        else:
            self.timerIMUData.stop()
            self.dataCollectionStarted = False
            self.btnIMUDataStartStop.setText('Start')

            self.generatePositionDataAndShow()

    def takeIntegral(self, f, t):
        integF = np.zeros(len(t))
        s = ((f[0] + 0) / 2.0) * t[0]
        integF[0] = s

        i = 1
        while i < len(t):
            dt = (t[i] - t[i - 1])
            s += ((f[i] + f[i - 1]) / 2.0) * dt
            integF[i] = s
            i += 1

        return integF


    # def applyBandPassFilter(self, t, fn):
    #     N = len(t)
    #     T = t[-1] #duration of recording
    #     Ts = T / (N-1)
    #     fs = 1 / Ts
    #
    #     w = np.fft.fft(fn)
    #     freqs = np.fft.fftfreq(N, Ts)
    #
    #     idx = np.argmax(np.abs(w))
    #     f0 = abs(freqs[idx])
    #     #f0 = abs(freq * N * df)
    #     if f0==0: f0 = 1.0
    #     print f0
    #
    #     order = 6
    #     #cutoff = f0*5  # desired cutoff frequency of the filter, Hz
    #     lowcut = f0 / 2
    #     highcut = 2 * f0
    #
    #     fnFiltered = butter_bandpass_filter(fn, lowcut, highcut, fs, order)
    #
    #     return fnFiltered

    def generatePositionDataAndShow(self):
        npData = np.array(self.collectedData)

        t = npData[:, self.tIndex] / 1000.0  # convert seconds
        LAx_m = npData[:, self.LAxIndex]
        LAy_m = npData[:, self.LAyIndex]
        LAz_m = npData[:, self.LAzIndex]


        self._updateLinAccDrift()
        # print self.LAxDrift, self.LAyDrift, self.LAzDrift
        LAx = LAx_m - self.LAxDrift
        LAy = LAy_m - self.LAyDrift
        LAz = LAz_m - self.LAzDrift


        Vx = self.takeIntegral(LAx, t)
        Vy = self.takeIntegral(LAy, t)
        Vz = self.takeIntegral(LAz, t)

        Px = self.takeIntegral(Vx, t)
        Py = self.takeIntegral(Vy, t)
        Pz = self.takeIntegral(Vz, t)

        fig = plt.figure()
        ax = fig.gca(projection='3d')

        ax.plot(Px, Py, Pz)
        ax.plot([Px[0]], [Py[0]], [Pz[0]], marker='o', color='r')
        ax.plot([Px[-1]], [Py[-1]], [Pz[-1]], marker='o', color='green')

        # Create cubic bounding box to simulate equal aspect ratio
        max_range = np.array([Px.max() - Px.min(), Py.max() - Py.min(), Pz.max() - Pz.min()]).max()
        Xb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][0].flatten() + 0.5 * (Px.max() + Px.min())
        Yb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][1].flatten() + 0.5 * (Py.max() + Py.min())
        Zb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][2].flatten() + 0.5 * (Pz.max() + Pz.min())
        # Comment or uncomment following both lines to test the fake bounding box:
        for xb, yb, zb in zip(Xb, Yb, Zb):
            ax.plot([xb], [yb], [zb], 'w')




        plt.figure()
        plt.subplot(3, 1, 1)
        plt.plot(t, LAx, 'b-', label='data')
        plt.plot(t, LAx_m, 'r-',  label='filtered data')


        plt.subplot(3, 1, 2)
        plt.plot(t, LAy, 'b-', label='data')
        plt.plot(t, LAy_m, 'r-',  label='filtered data')


        plt.subplot(3, 1, 3)
        plt.plot(t, LAz, 'b-', label='data')
        plt.plot(t, LAz_m, 'r-',  label='filtered data')


        plt.show()

    def on_btnSaveIMUData_clicked(self):
        folderName = 'data/'
        fileName = folderName + 'data_' + str(datetime.now())
        with open(fileName, 'w') as output:
            for data in self.collectedData:
                for item in data:
                    output.write(str(item) + ' ')
                output.write('\n')

    def on_timerIMUData_timeout(self):

        Ax, Ay, Az, Mx, My, Mz, Gx, Gy, Gz, Eh, Er, Ep, Qx, Qy, Qz, Qw, LAx, LAy, LAz = self.bno.read_all()

        self.tbEulerAngles.setText('{0:10.2F},{1:10.2F},{2:10.2F}'.format(Eh, Er, Ep))
        self.tbQuaterion.setText('{0:10.5F},{1:10.5F},{2:10.5F},{2:10.5F}'.format(Qx, Qy, Qz, Qw))
        self.tbGyroscope.setText('{0:10.2F},{1:10.2F},{2:10.2F}'.format(Gx, Gy, Gz))
        self.tbAccelerometer.setText('{0:10.2F},{1:10.2F},{2:10.2F}'.format(Ax, Ay, Az))
        self.tbLinearAccel.setText('{0:10.2F},{1:10.2F},{2:10.2F}'.format(LAx, LAy, LAz))
        self.tbMagnetometer.setText('{0:10.2F},{1:10.2F},{2:10.2F}'.format(Mx, My, Mz))

        currentMeasurementTime = int(time.time() * 1000)
        elapsedTime = currentMeasurementTime - self.beginMeasurementTime

        gyroTotal = abs(Gx) + abs(Gy) + abs(Gz)
        if gyroTotal < 0.1: #and len(self.collectedData) == 0:
            self.zeroGyroData.append(
                [elapsedTime, Eh, Er, Ep, Qx, Qy, Qz, Qw, Gx, Gy, Gz, Ax, Ay, Az, LAx, LAy, LAz, Mx, My, Mz])
        else:
            if len(self.collectedData) == 0:
                self.beginMeasurementTime = currentMeasurementTime
                elapsedTime = 0

            self.collectedData.append(
                [elapsedTime, Eh, Er, Ep, Qx, Qy, Qz, Qw, Gx, Gy, Gz, Ax, Ay, Az, LAx, LAy, LAz, Mx, My, Mz])


        # dt = currentMeasurementTime - self.prevMeasurementTime
        # self.prevMeasurementTime = currentMeasurementTime
        # print dt, 1000.0 / dt
