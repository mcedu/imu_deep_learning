import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D
import numpy as np
import os
import time
from PySide2 import QtWidgets
from datetime import datetime

from PySide2.QtCore import QFile, QObject, QTimer
from PySide2.QtUiTools import QUiLoader
from PySide2.QtGui import QKeySequence
from PySide2.QtWidgets import QShortcut
# from Adafruit_BNO055 import BNO055
from BNO055_UPDATE import BNO055
import serial.tools.list_ports
from scipy.signal import butter, lfilter, freqz



class FormIMUControl(QObject):
    def __init__(self, parent=None):
        super(FormIMUControl, self).__init__()
        self.loadUiFile()

        self.bno = None
        self.connected = False
        self.ports = []
        self.update_port_list()

        self.timerCalib = QTimer(self)
        self.timerCalib.setInterval(1000)
        self.timerCalib.timeout.connect(self.on_calibTimer_timeout)

        self.timerIMUData = QTimer(self)
        self.timerIMUData.setInterval(1)
        self.timerIMUData.timeout.connect(self.on_timerIMUData_timeout)

        self.beginMeasurementTime = 0
        self.prevMeasurementTime = 0
        self.collectedData = []
        self.zeroGyroData = []
        self.dataCollectionStarted = False

        self.tIndex = 0
        self.LAxIndex = 14
        self.LAyIndex = 15
        self.LAzIndex = 16
        self.LAxDrift = 0.0
        self.LAyDrift = 0.0
        self.LAzDrift = 0.0

        self.AxIndex = 11
        self.AyIndex = 12
        self.AzIndex = 13
        self.AxDrift = 0.0
        self.AyDrift = 0.0
        self.AzDrift = 0.0

        self.lastVelProfile = None
        self.lastPosProfile = None

        self.fig = None
        self.saveFileInfoCounter = 0
        self.lastImuFile = None

    def loadUiFile(self):
        ui_file = QFile('form_imu_control.ui')
        ui_file.open(QFile.ReadOnly)
        loader = QUiLoader()
        self.window = loader.load(ui_file)
        ui_file.close()

        self.lblConnection = self.window.findChild(QtWidgets.QLabel, 'lblConnection')
        self.btnConnect = self.window.findChild(QtWidgets.QPushButton, 'btnConnect')
        self.btnConnect.clicked.connect(self.on_btnConnect_click)

        self.btnDisconnect = self.window.findChild(QtWidgets.QPushButton, 'btnDisconnect')
        self.btnDisconnect.clicked.connect(self.on_btnDisconnect_click)

        self.cbPort = self.window.findChild(QtWidgets.QComboBox, "cbPorts")

        self.tbCalibSytem = self.window.findChild(QtWidgets.QLineEdit, 'tbCalibSystem')
        self.tbCalibGyro = self.window.findChild(QtWidgets.QLineEdit, 'tbCalibGyro')
        self.tbCalibAcc = self.window.findChild(QtWidgets.QLineEdit, 'tbCalibAcc')
        self.tbCalibMag = self.window.findChild(QtWidgets.QLineEdit, 'tbCalibMag')

        self.btnCalibStart = self.window.findChild(QtWidgets.QPushButton, 'btnCalibStart')
        self.btnCalibStart.clicked.connect(self.on_btnCalibStart_click)

        self.btnCalibStop = self.window.findChild(QtWidgets.QPushButton, 'btnCalibStop')
        self.btnCalibStop.clicked.connect(self.on_btnCalibStop_click)

        self.btnCalibSave = self.window.findChild(QtWidgets.QPushButton, 'btnCalibSave')
        self.btnCalibSave.clicked.connect(self.on_btnCalibSave_click)

        self.btnCalibRestore = self.window.findChild(QtWidgets.QPushButton, 'btnCalibRestore')
        self.btnCalibRestore.clicked.connect(self.on_btnCalibRestore_click)

        self.btnCalculateLinAccDrift = self.window.findChild(QtWidgets.QPushButton, 'btnCalculateLinAccDrift')
        self.btnCalculateLinAccDrift.clicked.connect(self.on_btnCalculateLinAccDrift_click)

        self.tbEulerAngles = self.window.findChild(QtWidgets.QLineEdit, 'tbEulerAngles')
        self.tbQuaterion = self.window.findChild(QtWidgets.QLineEdit, 'tbQuaterion')
        self.tbGyroscope = self.window.findChild(QtWidgets.QLineEdit, 'tbGyroscope')
        self.tbAccelerometer = self.window.findChild(QtWidgets.QLineEdit, 'tbAccelerometer')
        self.tbLinearAccel = self.window.findChild(QtWidgets.QLineEdit, 'tbLinearAccel')
        self.tbMagnetometer = self.window.findChild(QtWidgets.QLineEdit, 'tbMagnetometer')

        self.tbFolder = self.window.findChild(QtWidgets.QLineEdit, 'tbFolder')
        self.lbFolderDescription = self.window.findChild(QtWidgets.QLabel, 'lbFolderDescription')
        self.lbFolderDescription.setText('')
        self.tbFolder.editingFinished.connect(self.update_folder_description)
        self.tbFolder.setText("imu_mnist_2d_dataset/")

        self.btnIMUDataStartStop = self.window.findChild(QtWidgets.QPushButton, 'btnIMUDataStartStop')
        self.btnIMUDataStartStop.clicked.connect(self.on_btnIMUDataStartStop_clicked)
        shortcut = QShortcut(QKeySequence("Spacebar"), self.btnIMUDataStartStop,
                             self.on_btnIMUDataStartStop_clicked)

        self.btnSaveIMUData = self.window.findChild(QtWidgets.QPushButton, 'btnSaveIMUData')
        self.btnSaveIMUData.clicked.connect(self.on_btnSaveIMUData_clicked)
        shortcut = QShortcut(QKeySequence("Ctrl+S"), self.btnSaveIMUData,
                             self.on_btnSaveIMUData_clicked)

        self.btnDeleteLastData = self.window.findChild(QtWidgets.QPushButton, 'btnDeleteLastData')
        self.btnDeleteLastData.clicked.connect(self.on_btnDeleteLastData_clicked)
        shortcut = QShortcut(QKeySequence("F"), self.btnDeleteLastData,
                             self.on_btnDeleteLastData_clicked)

    def show(self):
        self.window.show()

    def update_port_list(self):
        self.ports = []
        self.cbPort.clear()
        for port in serial.tools.list_ports.grep(".*"):
            self.ports.append(port)
            self.cbPort.addItem(port.description)

    def update_folder_description(self):
        folder = self.tbFolder.text()
        if os.path.isdir(folder):
            items = os.listdir(folder)
            imu_num = 0
            png_num = 0
            for x in items:
                if x.endswith('.imu'):
                    imu_num += 1
                elif x.endswith('.png'):
                    png_num += 1
            self.lbFolderDescription.setText('{0} .imu files, {1} .png files'.format(
                imu_num, png_num))
        else:
            self.lbFolderDescription.setText('No such folder {0}'.format(folder))

    def on_btnConnect_click(self):
        index = self.cbPort.currentIndex()
        port = self.ports[index]
        self.bno = BNO055(serial_port=port.device, rst=None)
        self.connected = self.bno.begin()

        if self.connected:
            self.lblConnection.setText('ON')
        else:
            self.lblConnection.setText('OFF')

    def on_btnDisconnect_click(self):
        self.bno = None
        self.connected = False

        self.lblConnection.setText('OFF')
        self.timerCalib.stop()

    def on_calibTimer_timeout(self):
        sys, gyro, accel, mag = self.bno.get_calibration_status()
        for text, value in [(self.tbCalibSytem, sys), (self.tbCalibGyro, gyro),
                            (self.tbCalibAcc, accel), (self.tbCalibMag, mag)]:
            text.setText(str(value))
            if value == 0:
                text.setStyleSheet("QLineEdit { background: rgb(255, 0, 0);}")
            elif value == 3:
                text.setStyleSheet("QLineEdit { background: rgb(0, 255, 0);}")
            else:
                text.setStyleSheet("QLineEdit { background: rgb(255, 255, 255);}")

    def on_btnCalibStart_click(self):
        self.timerCalib.start()

    def on_btnCalibStop_click(self):
        self.timerCalib.stop()

    def on_btnCalibSave_click(self):
        calib_data = self.bno.get_calibration()

        folderName = 'calibration/'
        fileName = folderName + 'calibration_data'
        with open(fileName, 'w') as output:
            for item in calib_data:
                output.write(str(item) + ' ')

        #print calib_data

    def on_btnCalibRestore_click(self):
        folderName = 'calibration/'
        fileName = folderName + 'calibration_data'

        calib_data = []
        with open(fileName, 'r') as inputFile:
            dataLine = inputFile.readline()
            dataAsStrList = dataLine.split()

            for dStr in dataAsStrList:
                calib_data.append(int(dStr))

            self.bno.set_calibration(calib_data)

    def _updateLinAccDrift(self):
        npData = np.array(self.zeroGyroData)
        LAx = npData[:, self.LAxIndex]
        LAy = npData[:, self.LAyIndex]
        LAz = npData[:, self.LAzIndex]

        self.LAxDrift = np.average(LAx)
        self.LAyDrift = np.average(LAy)
        self.LAzDrift = np.average(LAz)

    def _updateAccDrift(self):
        npData = np.array(self.zeroGyroData)
        Ax = npData[:, self.AxIndex]
        Ay = npData[:, self.AyIndex]
        Az = npData[:, self.AzIndex]

        self.AxDrift = np.average(Ax)
        self.AyDrift = np.average(Ay)
        self.AzDrift = np.average(Az)

    def on_btnCalculateLinAccDrift_click(self):
        self._updateLinAccDrift()

    def on_btnIMUDataStartStop_clicked(self):
        if not self.dataCollectionStarted:
            self.beginMeasurementTime = int(time.time() * 1000)
            self.prevMeasurementTime = self.beginMeasurementTime

            self.timerIMUData.start()
            self.collectedData = []
            self.zeroGyroData = []
            self.dataCollectionStarted = True
            self.btnIMUDataStartStop.setText('Stop')

        else:
            self.timerIMUData.stop()
            self.dataCollectionStarted = False
            self.btnIMUDataStartStop.setText('Start')

            self.generatePositionDataAndShow()

    def takeIntegral(self, f, t):
        integF = np.zeros(len(t))
        s = ((f[0] + 0) / 2.0) * t[0]
        integF[0] = s

        i = 1
        while i < len(t):
            dt = (t[i] - t[i - 1])
            s += ((f[i] + f[i - 1]) / 2.0) * dt
            integF[i] = s
            i += 1

        return integF

    def generatePositionDataAndShow(self):
        npData = np.array(self.collectedData)

        t = npData[:, self.tIndex] / 1000.0  # convert seconds

        #Linear Acceleration
        # LAx_m = npData[:, self.LAxIndex]
        # LAy_m = npData[:, self.LAyIndex]
        # LAz_m = npData[:, self.LAzIndex]
        # self._updateLinAccDrift()
        # LAx = LAx_m - self.LAxDrift
        # LAy = LAy_m - self.LAyDrift
        # LAz = LAz_m - self.LAzDrift
        Ax_m = npData[:, self.AxIndex]
        Ay_m = npData[:, self.AyIndex]
        Az_m = npData[:, self.AzIndex]
        self._updateAccDrift()
        Ax = Ax_m - self.AxDrift
        Ay = Ay_m - self.AyDrift
        Az = Az_m - self.AzDrift

        Vx = self.takeIntegral(Ax, t)
        Vy = self.takeIntegral(Ay, t)
        Vz = self.takeIntegral(Az, t)

        Px = self.takeIntegral(Vx, t)
        Py = self.takeIntegral(Vy, t)
        Pz = self.takeIntegral(Vz, t)

        self.lastVelProfile = np.array([Vx, Vy, Vz])
        self.lastPosProfile = np.array([Px, Py, Pz])

        self.fig = plt.figure()
        #ax = self.fig.gca()
        #ax.get_xaxis().set_ticks([])
        #ax.get_yaxis().set_ticks([])
        #ax.axis('off')
        #ax.plot(Px, Py, linewidth = 10, color = 'black')

        ax = self.fig.gca(projection='3d')
        ax.set_xlim(xmin=-1, xmax=1)
        ax.set_ylim(ymin=-1, ymax=1)
        ax.set_zlim(zmin=-1, zmax=1)
        ax.plot(Px, Py, Pz, color='black')
        ax.plot([Px[0]], [Py[0]], [Pz[0]], marker='o', color='r')
        ax.plot([Px[-1]], [Py[-1]], [Pz[-1]], marker='o', color='green')

        # ax.plot([Px[0]], [Py[0]], marker='o', color='r')
        # ax.plot([Px[-1]], [Py[-1]], marker='o', color='green')

        # Create cubic bounding box to simulate equal aspect ratio
        #max_range = np.array([Px.max() - Px.min(), Py.max() - Py.min(), Pz.max() - Pz.min()]).max()
        #Xb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][0].flatten() + 0.5 * (Px.max() + Px.min())
        #Yb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][1].flatten() + 0.5 * (Py.max() + Py.min())
        #Zb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][2].flatten() + 0.5 * (Pz.max() + Pz.min())
        # Comment or uncomment following both lines to test the fake bounding box:
        #for xb, yb, zb in zip(Xb, Yb, Zb):
        #    ax.plot([xb], [yb], [zb], 'w')

        plt.show()

        fig = plt.figure()
        ax = fig.gca()

        ax.set_xlim(xmin=-1, xmax=1)
        ax.set_ylim(ymin=-1, ymax=1)

        ax.plot(Py, Pz, linewidth=10, color='blue')
        # for xb, yb, zb in zip(Xb, Yb, Zb):
        #    ax.plot([yb], [zb], [xb], 'w')

        ax.plot(Px, Pz, linewidth=10, color='green')
        # for xb, yb, zb in zip(Xb, Yb, Zb):
        #    ax.plot([xb], [zb], [yb], 'w')

        ax.plot(Px, Py, linewidth=10, color='red')
        #for xb, yb, zb in zip(Xb, Yb, Zb):
        #    ax.plot([xb], [yb], [zb], 'w')

        plt.show()

    def on_btnSaveIMUData_clicked(self):
        folderName = self.tbFolder.text()
        try:
            os.makedirs(folderName)
        except OSError as e:
            pass
        #suffix = str(datetime.now()).split('.')[0]
        suffix = datetime.now().strftime("%Y%m%d_%H%M%S_%f")

        fileName = os.path.join(folderName, 'data_' + suffix + '.imu')
        with open(fileName, 'w') as output:
            output.write('sample_count:{0} record_time_stamp:{1} LADrift:[{2},{3},{4}]\n'.format(len(self.collectedData), self.beginMeasurementTime, self.LAxDrift, self.LAyDrift, self.LAzDrift))

            fieldNames = ['ETime', 'Eh', 'Er', 'Ep', 'Qx', 'Qy', 'Qz', 'Qw', 'Gx', 'Gy', 'Gz', 'Ax', 'Ay', 'Az', 'LAx', 'LAy', 'LAz', 'Mx', 'My', 'Mz', 'Vx', 'Vy', 'Vz', 'Px', 'Py', 'Pz']

            for field in fieldNames:
                output.write('{0:<25} '.format(field))

            output.write('\n')

            for i, data in enumerate(self.collectedData):
                for item in data:
                    output.write('{0:<25} '.format(item))

                for k in range(3):
                    output.write('{0:<25} '.format(self.lastVelProfile[k, i]))

                for k in range(3):
                    output.write('{0:<25} '.format(self.lastPosProfile[k, i]))

                output.write('\n')

        #self.fig.savefig(fileName + '.png', bbox_inches='tight')
        Px = self.lastPosProfile[0, :]
        Py = self.lastPosProfile[1, :]
        Pz = self.lastPosProfile[2, :]
        self.save2DFigure(Px, Py, Pz, fileName)

        self.lastImuFile = fileName

        self.saveFileInfoCounter += 1

        print('[{0:3d}]  --  {1} saved'.format(self.saveFileInfoCounter, fileName))
        self.update_folder_description()

    def save2DFigure(self, Px, Py, Pz, baseFileName):
        # Create cubic bounding box to simulate equal aspect ratio
        max_range = np.array([Px.max() - Px.min(), Py.max() - Py.min(), Pz.max() - Pz.min()]).max()
        Xb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][0].flatten() + 0.5 * (Px.max() + Px.min())
        Yb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][1].flatten() + 0.5 * (Py.max() + Py.min())
        Zb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][2].flatten() + 0.5 * (Pz.max() + Pz.min())
        # Comment or uncomment following both lines to test the fake bounding box:
        fig = plt.figure()
        ax = fig.gca()
        ax.plot(Px, Py, linewidth=10, color='black')

        for xb, yb, zb in zip(Xb, Yb, Zb):
            ax.plot([xb], [yb], [zb], 'w')

        fig.savefig(baseFileName + '_XY.png')
        plt.close('all')

        fig = plt.figure()
        ax = fig.gca()
        ax.plot(Px, Pz, linewidth=10, color='black')

        for xb, yb, zb in zip(Xb, Yb, Zb):
            ax.plot([xb], [yb], [zb], 'w')

        fig.savefig(baseFileName + '_XZ.png')
        plt.close('all')

        fig = plt.figure()
        ax = fig.gca()
        ax.plot(Py, Pz, linewidth=10, color='black')

        for xb, yb, zb in zip(Xb, Yb, Zb):
            ax.plot([xb], [yb], [zb], 'w')

        fig.savefig(baseFileName + '_YZ.png')
        plt.close('all')

    def on_btnDeleteLastData_clicked(self):
        if self.lastImuFile is not None:
            try:
                os.remove(self.lastImuFile)
                print("deleted " + self.lastImuFile)
            except OSError as e:
                pass
            for suffix in ('_XY.png', '_XZ.png', '_YZ.png'):
                try:
                    png_file = self.lastImuFile + suffix
                    os.remove(png_file)
                    print("deleted " + png_file)
                except OSError as e:
                    pass
            self.lastImuFile = None
            self.update_folder_description()

    def on_timerIMUData_timeout(self):

        Ax, Ay, Az, Mx, My, Mz, Gx, Gy, Gz, Eh, Er, Ep, Qx, Qy, Qz, Qw, LAx, LAy, LAz = self.bno.read_all()

        self.tbEulerAngles.setText('{0:10.2F},{1:10.2F},{2:10.2F}'.format(Eh, Er, Ep))
        self.tbQuaterion.setText('{0:10.5F},{1:10.5F},{2:10.5F},{2:10.5F}'.format(Qx, Qy, Qz, Qw))
        self.tbGyroscope.setText('{0:10.2F},{1:10.2F},{2:10.2F}'.format(Gx, Gy, Gz))
        self.tbAccelerometer.setText('{0:10.2F},{1:10.2F},{2:10.2F}'.format(Ax, Ay, Az))
        self.tbLinearAccel.setText('{0:10.2F},{1:10.2F},{2:10.2F}'.format(LAx, LAy, LAz))
        self.tbMagnetometer.setText('{0:10.2F},{1:10.2F},{2:10.2F}'.format(Mx, My, Mz))

        currentMeasurementTime = int(time.time() * 1000)
        elapsedTime = currentMeasurementTime - self.beginMeasurementTime

        gyroTotal = abs(Gx) + abs(Gy) + abs(Gz)
        if gyroTotal < 0.1 and len(self.zeroGyroData) == 0: #and len(self.collectedData) == 0:
            self.zeroGyroData.append(
                [elapsedTime, Eh, Er, Ep, Qx, Qy, Qz, Qw, Gx, Gy, Gz, Ax, Ay, Az, LAx, LAy, LAz, Mx, My, Mz])
        else:
            if len(self.collectedData) == 0:
                self.beginMeasurementTime = currentMeasurementTime
                elapsedTime = 0

            self.collectedData.append(
                [elapsedTime, Eh, Er, Ep, Qx, Qy, Qz, Qw, Gx, Gy, Gz, Ax, Ay, Az, LAx, LAy, LAz, Mx, My, Mz])


        # dt = currentMeasurementTime - self.prevMeasurementTime
        # self.prevMeasurementTime = currentMeasurementTime
        # print dt, 1000.0 / dt
