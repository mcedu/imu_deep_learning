import sys
from PySide2 import QtWidgets

from FormIMUControl_2DPos import FormIMUControl

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    w = FormIMUControl()
    w.show()

    sys.exit(app.exec_())
