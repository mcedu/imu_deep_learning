import os
from os.path import expanduser

import imageio
from six.moves import cPickle as pickle
import numpy as np
from sklearn.cross_validation import train_test_split


class PickleHelper:


    def __init__(self, rootFolder, im_height = 28, im_width = 28, pixelDepth=255):
        self.rootFolder = rootFolder
        self.imHeight = im_height
        self.imWidth = im_width
        self.pixelDepth = pixelDepth
        self.totalNumberOfImages = 0

        self.dataFolders = [
            os.path.join(rootFolder, d) for d in sorted(os.listdir(rootFolder))
            if os.path.isdir(os.path.join(rootFolder, d))
        ]

        self.numClasses = len(self.dataFolders)
        self.dataset = None
        self.labels = None
        self.trainData = None
        self.trainLabels = None
        self.validData = None
        self.validLabels = None
        self.testData = None
        self.testLabels = None

    def _loadClass(self, folder):
        image_files = os.listdir(folder)
        dataset = np.ndarray(shape=(len(image_files), self.imHeight, self.imWidth),
                             dtype=np.float32)

        num_images = 0
        for image in image_files:
            image_file = os.path.join(folder, image)

            try:
                #TODO: Adjust according to channel count of the file
                image_data = (imageio.imread(image_file).astype(float) - self.pixelDepth / 2) / self.pixelDepth
                if image_data.shape != (self.imHeight, self.imWidth):
                    raise Exception('Unexpected image shape: %s' % str(image_data.shape))
                dataset[num_images, :, :] = image_data
                num_images += 1
            except (IOError, ValueError) as e:
                print('Could not read:', image_file, ':', e, '- it\'s ok, skipping.')

        dataset = dataset[0:num_images, :, :]
        print('Full dataset tensor:', dataset.shape)
        print('Mean:', np.mean(dataset))
        print('Standard deviation:', np.std(dataset))
        return dataset, num_images

    def pickleForEachClass(self):
        dataset_names = []
        totalNumberOfImages = 0
        for folder in self.dataFolders:
            set_filename = folder + '.pickle'
            dataset_names.append(set_filename)
            if os.path.exists(set_filename):
                print('%s already present - Skipping pickling.' % set_filename)
            else:
                print('Pickling %s.' % set_filename)
                dataset, numImages = self._loadClass(folder)
                try:
                    with open(set_filename, 'wb') as f:
                        pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
                    totalNumberOfImages += numImages
                except Exception as e:
                    print('Unable to save data to', set_filename, ':', e)

        self.datasetNames = dataset_names
        self.totalNumberOfImages = totalNumberOfImages

    def _makeArrays(self, count):
        if count:
            dataset = np.ndarray((count, self.imHeight, self.imWidth), dtype=np.float32)
            labels = np.ndarray(count, dtype=np.int32)
        else:
            dataset, labels = None, None

        return dataset, labels

    def mergeDataSets(self, totalCount=0):

        if totalCount == 0:
            totalCount = self.totalNumberOfImages
        else:
            if totalCount > self.totalNumberOfImages:
                assert 1, 'Total Count cannot be bigger than total number of images!'

        dataset, labels = self._makeArrays(totalCount)
        count_per_class = totalCount / self.numClasses
        start = 0
        end = count_per_class

        for label, pickle_file in enumerate(self.datasetNames):
            try:
                with open(pickle_file, 'rb') as f:
                    classSet = pickle.load(f)
                    np.random.shuffle(classSet)

                    classSetPart = classSet[0:count_per_class, :, :]
                    dataset[start:end, :, :] = classSetPart
                    labels[start:end] = label
                    start += count_per_class
                    end += count_per_class

            except Exception as e:
                print('Unable to process data from', pickle_file, ':', e)
                raise

        self.dataset = dataset
        self.labels = labels
        # return dataset, labels

    def divideDataSetRandomly(self, trainRatio=0.7, validRatio=0.15, testRatio=0.15):
        trainData, validTestData, trainLabels, validTestLabels = \
            train_test_split(self.dataset, self.labels,
                             train_size=trainRatio, stratify=self.labels)

        validRatio = validRatio / (validRatio + testRatio)
        validData, testData, validLabels, testLabels = \
            train_test_split(validTestData, validTestLabels,
                             train_size=validRatio, stratify=validTestLabels)

        self.trainData = trainData
        self.trainLabels = trainLabels
        self.validData = validData
        self.validLabels = validLabels
        self.testData = testData
        self.testLabels = testLabels
        # return trainData, trainLabels, validData, validLabels, testData, testLabels

    def saveAllDataSets(self, fileName):
        pickle_file = os.path.join(self.rootFolder, fileName + '.pickle')

        try:
            with open(pickle_file, 'wb') as f:
                save = {
                    'trainData'     : self.trainData,
                    'trainLabels'   : self.trainLabels,
                    'validData'     : self.validData,
                    'validLabels'   : self.validLabels,
                    'testData'      : self.testData,
                    'testLabels'    : self.testLabels,
                }
                pickle.dump(save,f, pickle.HIGHEST_PROTOCOL)
        except Exception as e:
            print('Unable to save data to', pickle_file, ':', e)
            raise

    def loadAllDataSets(self, fileName):
        pickle_file = os.path.join(self.rootFolder, fileName + '.pickle')
        with open(pickle_file,'rb') as f:
            save = pickle.load(f)
            self.trainData     = save['trainData']
            self.trainLabels   = save['trainLabels']
            self.validData     = save['validData']
            self.validLabels   = save['validLabels']
            self.testData      = save['testData']
            self.testLabels    = save['testLabels']
            del save
            print('Training set : ', self.trainData.shape, self.trainLabels.shape)
            print('Validation set : ', self.validData.shape, self.validLabels.shape)
            print('Test set : ', self.testData.shape, self.testLabels.shape)





if __name__ == '__main__':
    HOME = expanduser("~")
    root = HOME + '/tensorflow/bno_data_192_192/'
    imHeight = 192
    imWidth  = 192
    finalRecordFileName = 'bno_data'


    pHelper = PickleHelper(root, im_height=imHeight, im_width=imWidth)
    pHelper.pickleForEachClass()
    pHelper.mergeDataSets()
    pHelper.divideDataSetRandomly(trainRatio=0.9, validRatio=0.05, testRatio=0.05)
    pHelper.saveAllDataSets(fileName=finalRecordFileName)


    # pLoader = PickleHelper(root)
    # pLoader.loadAllDataSets(fileName=finalRecordFileName)
    # print pLoader.trainData