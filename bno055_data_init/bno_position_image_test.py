import matplotlib.pyplot as plt
from os.path import expanduser

from tensorflow import keras
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.models import Sequential
# print(tf.__version__)
from tensorflow.python.keras.layers import Dropout

from PickleHelper import PickleHelper

HOME = expanduser("~")
root = HOME + '/tensorflow/bno_data_192_192/'
pLoader = PickleHelper(root)
pLoader.loadAllDataSets('bno_data')

class_names = ['0', '1', '2', '3']
batch_size = 32
num_classes = len(class_names)
epochs = 10
img_x, img_y = 192, 192
input_shape = (img_x, img_y, 1)

#fashion_mnist = keras.datasets.fashion_mnist

train_images = pLoader.trainData
train_labels = pLoader.trainLabels
valid_images = pLoader.validData
valid_labels = pLoader.validLabels
test_images = pLoader.testData
test_labels = pLoader.testLabels

#(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()



train_images = train_images.reshape(train_images.shape[0], img_x, img_y, 1)
valid_images = valid_images.reshape(valid_images.shape[0], img_x, img_y, 1)
test_images = test_images.reshape(test_images.shape[0], img_x, img_y, 1)

train_labels = keras.utils.to_categorical(train_labels, num_classes)
valid_labels = keras.utils.to_categorical(valid_labels, num_classes)
test_labels = keras.utils.to_categorical(test_labels, num_classes)


# model = keras.Sequential([
#     keras.layers.Conv2D(filters=32, kernel_size=(5,5), strides=(1,1), activation=keras.activations.relu, input_shape=input_shape),
#     keras.layers.MaxPooling2D(pool_size=(2,2)),
#     keras.layers.Conv2D(filters=64, kernel_size=(5, 5), activation=keras.activations.relu),
#     keras.layers.MaxPooling2D(pool_size=(2, 2)),
#     keras.layers.Flatten(),
#     keras.layers.Dense(100, activation=tf.nn.relu),
#     keras.layers.Dense(num_classes, activation=tf.nn.softmax)
# ])

model = Sequential()

model.add(Conv2D(filters=32, kernel_size=(5, 5), strides=(1, 1),
                 activation='relu', input_shape=input_shape, padding='same' ))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

model.add(Conv2D(64, (5, 5), activation='relu', padding='same'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dropout(0.25))

model.add(Dense(100, activation='relu'))
model.add(Dropout(0.25))

model.add(Dense(num_classes, activation='softmax'))

model.compile(optimizer=keras.optimizers.Adam(),
              loss=keras.losses.categorical_crossentropy,
              metrics=['accuracy'])

model.summary()


class AccuracyHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.acc = []

    def on_epoch_end(self, batch, logs={}):
        self.acc.append(logs.get('acc'))

history = AccuracyHistory()


model.fit(train_images, train_labels,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          callbacks=[history],
          validation_data=(valid_images, valid_labels)
          )

score = model.evaluate(test_images, test_labels, verbose=1)

print('Test loss:', score[0])
print('Test accuracy:', score[1])
plt.plot(range(1, epochs + 1), history.acc)
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.show()
