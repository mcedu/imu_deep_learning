import glob
import os

from PIL import Image


def checkDirExists(folderName):
    if not os.path.exists(folderName):
        os.makedirs(folderName)
    else:
        content = os.listdir(folderName)
        assert len(content) == 0, 'FOLDER ' + folderName + ' MUST BE EMPTY!!'


def processImages(rootFolder, subDir, imHeight, imWidth ):
    originalImageFolder = rootFolder + '/' + subDir + '/'
    if not os.path.isdir(originalImageFolder):
        return

    originalImagePaths = glob.glob(originalImageFolder + '*.png')

    processedImageFolder = rootFolder + '/processed/' + subDir + '/'
    checkDirExists(processedImageFolder)

    for originalImageFilePath in originalImagePaths:
        originalImageFileName = originalImageFilePath.split('/')[-1]

        imageObj = Image.open(originalImageFilePath)
        croppedObj = imageObj.crop((102, 61, 720, 539))

        resizedObj = croppedObj.resize((imHeight,imWidth), Image.ANTIALIAS)
        # resizedObj.save(reziedFolder + originalImageFileName)

        grayScaleObj = resizedObj.convert('L')
        # grayScaleObj.save(grayFolder + originalImageFileName)
        grayScaleObj.save(processedImageFolder + originalImageFileName)
        print(originalImageFileName + ': OK')


def processImagesRecursive(rootFolder, imHeight, imWidth):
    dirList = os.listdir(rootFolder)

    for dir in dirList:
        processImages(rootFolder, dir, imHeight=imHeight, imWidth=imWidth)


if __name__ == '__main__':
    ROOT_FOLDER = 'data'
    processImagesRecursive(ROOT_FOLDER, imHeight=28, imWidth=28)
