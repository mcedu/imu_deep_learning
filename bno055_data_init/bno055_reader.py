from __future__ import print_function
import argparse
import curses
import numpy as np
import os
import pprint
import time
import serial.tools.list_ports

from datetime import datetime
from BNO055_UPDATE import BNO055
import matplotlib.pyplot as plt


fig = None
bno = None
connected = False
beginMeasurementTime = 0
prevMeasurementTime = 0
collectedData = []
zeroGyroData = []
dataCollectionStarted = False

tIndex = 0
LAxIndex = 14
LAyIndex = 15
LAzIndex = 16

LAxDrift = 0.0
LAyDrift = 0.0
LAzDrift = 0.0

lastVelProfile = None
lastPosProfile = None


def update_port_list():
    ports = []
    for port in serial.tools.list_ports.grep(".*"):
        ports.append(port)
    return ports


def connect(port):
    global bno
    global connected
    bno = BNO055(serial_port=port.device, rst=None)
    connected = bno.begin()


def disconnect():
    global bno
    global connected
    bno = None
    connected = False


def start_collection():
    global dataCollectionStarted
    global beginMeasurementTime
    global prevMeasurementTime
    global collectedData
    global zeroGyroData
    if not dataCollectionStarted:
        beginMeasurementTime = int(time.time() * 1000)
        prevMeasurementTime = beginMeasurementTime
        collectedData = []
        zeroGyroData = []
        dataCollectionStarted = True


def on_timeout():
    global beginMeasurementTime
    Ax, Ay, Az, Mx, My, Mz, Gx, Gy, Gz, Eh, Er, Ep, Qx, Qy, Qz, Qw, LAx, LAy, LAz = bno.read_all()

    record = {
        'tbEulerAngles': '{0:10.2F},{1:10.2F},{2:10.2F}'.format(Eh, Er, Ep),
        'tbQuaterion': '{0:10.5F},{1:10.5F},{2:10.5F},{2:10.5F}'.format(Qx, Qy, Qz, Qw),
        'tbGyroscope': '{0:10.2F},{1:10.2F},{2:10.2F}'.format(Gx, Gy, Gz),
        'tbAccelerometer': '{0:10.2F},{1:10.2F},{2:10.2F}'.format(Ax, Ay, Az),
        'tbLinearAccel': '{0:10.2F},{1:10.2F},{2:10.2F}'.format(LAx, LAy, LAz),
        'tbMagnetometer': '{0:10.2F},{1:10.2F},{2:10.2F}'.format(Mx, My, Mz),
    }
    pprint.pprint(record)

    currentMeasurementTime = int(time.time() * 1000)
    elapsedTime = currentMeasurementTime - beginMeasurementTime

    gyroTotal = abs(Gx) + abs(Gy) + abs(Gz)
    if gyroTotal < 0.1 and len(zeroGyroData) == 0: #and len(self.collectedData) == 0:
        zeroGyroData.append(
            [elapsedTime, Eh, Er, Ep, Qx, Qy, Qz, Qw, Gx, Gy, Gz, Ax, Ay, Az, LAx, LAy, LAz, Mx, My, Mz])
    else:
        if len(collectedData) == 0:
            beginMeasurementTime = currentMeasurementTime
            elapsedTime = 0

        collectedData.append(
            [elapsedTime, Eh, Er, Ep, Qx, Qy, Qz, Qw, Gx, Gy, Gz, Ax, Ay, Az, LAx, LAy, LAz, Mx, My, Mz])


def stop_collection():
    global dataCollectionStarted
    if dataCollectionStarted:
        dataCollectionStarted = False
        generatePositionDataAndShow()


def takeIntegral(f, t):
    integF = np.zeros(len(t))
    s = ((f[0] + 0) / 2.0) * t[0]
    integF[0] = s
    i = 1
    while i < len(t):
        dt = (t[i] - t[i - 1])
        s += ((f[i] + f[i - 1]) / 2.0) * dt
        integF[i] = s
        i += 1
    return integF


def _updateLinAccDrift():
    global LAxDrift
    global LAyDrift
    global LAzDrift
    npData = np.array(zeroGyroData)
    LAx = npData[:, LAxIndex]
    LAy = npData[:, LAyIndex]
    LAz = npData[:, LAzIndex]

    LAxDrift = np.average(LAx)
    LAyDrift = np.average(LAy)
    LAzDrift = np.average(LAz)


def generatePositionDataAndShow():
    global lastVelProfile
    global lastPosProfile
    global fig

    npData = np.array(collectedData)

    t = npData[:, tIndex] / 1000.0  # convert seconds
    LAx_m = npData[:, LAxIndex]
    LAy_m = npData[:, LAyIndex]
    LAz_m = npData[:, LAzIndex]

    _updateLinAccDrift()
    LAx = LAx_m - LAxDrift
    LAy = LAy_m - LAyDrift
    LAz = LAz_m - LAzDrift

    Vx = takeIntegral(LAx, t)
    Vy = takeIntegral(LAy, t)
    Vz = takeIntegral(LAz, t)

    Px = takeIntegral(Vx, t)
    Py = takeIntegral(Vy, t)
    Pz = takeIntegral(Vz, t)

    lastVelProfile = np.array([Vx, Vy, Vz])
    lastPosProfile = np.array([Px, Py, Pz])

    fig = plt.figure()
    ax = fig.gca()
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.axis('off')
    ax.plot(Px, Py, linewidth = 30, color = 'black')

    # ax.plot([Px[0]], [Py[0]], marker='o', color='r')
    # ax.plot([Px[-1]], [Py[-1]], marker='o', color='green')

    # Create cubic bounding box to simulate equal aspect ratio
    max_range = np.array([Px.max() - Px.min(), Py.max() - Py.min(), Pz.max() - Pz.min()]).max()
    Xb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][0].flatten() + 0.5 * (Px.max() + Px.min())
    Yb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][1].flatten() + 0.5 * (Py.max() + Py.min())
    Zb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][2].flatten() + 0.5 * (Pz.max() + Pz.min())
    # Comment or uncomment following both lines to test the fake bounding box:
    for xb, yb, zb in zip(Xb, Yb, Zb):
        ax.plot([xb], [yb], [zb], 'w')

    plt.show()


def save_imu_data():
    global fig
    folderName = 'test'
    try:
        os.makedirs(folderName)
    except OSError as e:
        pass
    suffix = datetime.now().strftime("%Y%m%d_%H%M%S_%f")
    fileName = os.path.join(folderName, 'data_' + suffix + '.imu')
    with open(fileName, 'w') as output:
        output.write('sample_count:{0} record_time_stamp:{1} LADrift:[{2},{3},{4}]\n'.format(len(collectedData), beginMeasurementTime, LAxDrift, LAyDrift, LAzDrift))
        fieldNames = ['ETime', 'Eh', 'Er', 'Ep', 'Qx', 'Qy', 'Qz', 'Qw', 'Gx', 'Gy', 'Gz', 'Ax', 'Ay', 'Az', 'LAx', 'LAy', 'LAz', 'Mx', 'My', 'Mz', 'Vx', 'Vy', 'Vz', 'Px', 'Py', 'Pz']

        for field in fieldNames:
            output.write('{0:<25} '.format(field))

        output.write('\n')

        for i, data in enumerate(collectedData):
            for item in data:
                output.write('{0:<25} '.format(item))

            for k in range(3):
                output.write('{0:<25} '.format(lastVelProfile[k, i]))

            for k in range(3):
                output.write('{0:<25} '.format(lastPosProfile[k, i]))

            output.write('\n')

    fig.savefig(fileName + '.png', bbox_inches='tight')

def main(win):
    parser = argparse.ArgumentParser()
    parser.add_argument('port')
    options = parser.parse_args()
    port_num = options.port
    for port in update_port_list():
        if port.name == port_num:
            break
    else:
        port = None
    #win = curses.initscr()
    win.nodelay(True)
    key = ""
    win.clear()

    connect(port)
    start_collection()
    while True:
        win.clear()
        on_timeout()
        curses.doupdate()
        try:
            key = win.getkey()
            # win.clear()
            if key == 'q':  # if key 'q' is pressed
                stop_collection()
                break  # finishing the loop
        except Exception:
            pass


if __name__ == '__main__':
    curses.wrapper(main)
