"""
    Matthew Christian
    2d_mnist_training.py
    Train a deep-learning model on 2D MNIST IMU data
"""
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import tensorflow as tf

from my_models import *
#from IPython.display import SVG
#from keras.utils.vis_utils import model_to_dot
from ann_visualizer.visualize import ann_viz
from PIL import Image
from time import time


NUM_CLASSES = 10
IMG_SHAPE = (28, 28, 1)

datadir = 'imu_mnist_2d_dataset/'


def parse_record(filename):
    features = {
        'name': tf.FixedLenFeature((), tf.string),
        'image': tf.FixedLenFeature((), tf.string),
        'label': tf.FixedLenFeature((), tf.int64),
    }
    parsed = tf.parse_single_example(filename, features)
    name = tf.decode_raw(parsed['name'], tf.uint8)
    #name.set_shape(50)
    d = {'name': name}
    image = tf.decode_raw(parsed['image'], np.float32)
    image = tf.reshape(image, tf.stack(IMG_SHAPE))
    X = image
    label = tf.one_hot(parsed['label'], NUM_CLASSES)
    d['CONV1_input'] = X
    return d, label


def image_input_fn(filenames, shuffle=False, batchsize=20, repeat=True):
    dataset = tf.data.TFRecordDataset(filenames)
    dataset = dataset.map(lambda fn: parse_record(fn))
    if shuffle:
        dataset = dataset.shuffle(batchsize)
    dataset = dataset.batch(batchsize)
    if repeat:
        dataset = dataset.repeat()
    iterator = dataset.make_initializable_iterator()
    return iterator


def direct_input_fn(group, input_type, shuffle=False, batchsize=20, repeat=True):
    from PIL import Image
    import imageio

    def gen_data():
        while True:
            for label in range(NUM_CLASSES):
                cls = str(label)
                group_dir = os.path.join(datadir, group, cls)
                if not os.path.exists(group_dir):
                    continue
                for basename in os.listdir(group_dir):
                    if not basename.endswith('.imu'):
                        continue
                    basename, _ = os.path.splitext(basename)
                    img_fn = os.path.join(group_dir, basename + '.png')
                    img = Image.open(img_fn).resize(IMG_SHAPE[:2], Image.ANTIALIAS).convert('L')
                    img = -1.0 * (np.array(img, dtype=np.float32) - 128) / 255.0
                    #img = (np.array(img, dtype=np.float32) - 128) / 255.0
                    #img = (imageio.imread(img_fn).astype(np.float32) - 127) / 255
                    # big_img = np.stack((img, img, img), axis=2)
                    x = img[:, :, np.newaxis]
                    #x = img[:, :, 0]
                    #y = [np.float(label)]
                    y = np.zeros((NUM_CLASSES,))
                    y[label] = 1
                    #print('x', x.shape, 'min', x.min(), 'max', x.max())
                    yield x, y
    dataset = tf.data.Dataset.from_generator(gen_data,
        output_types=(tf.float32, tf.float32),
        #output_shapes=((224, 224, 1), (10,))
        #output_shapes=(tf.TensorShape()(224, 224, 1), (10,))
    )
    if shuffle:
        dataset = dataset.shuffle(100)
    dataset = dataset.batch(batchsize)
    if repeat:
        dataset = dataset.repeat()
    iterator = dataset.make_initializable_iterator()
    return iterator


def fake_input_fn(shuffle=False, batchsize=20, repeat=True):
    from PIL import Image, ImageDraw
    def gen_data():
        while True:
            line = IMG_SHAPE[0] // 2
            halfline = line // 2
            for i in range(10):
                x = Image.new('L', IMG_SHAPE[:2])
                draw = ImageDraw.Draw(x)
                xx = 0
                yy = 0
                for j in range(i + 1):
                    xxx = xx + line
                    yyy = yy + (halfline + halfline * (-1)**j)
                    draw.line([xx, yy, xxx, yyy], fill=(255,), width=2)
                    xx = xxx
                    yy = yyy
                #x.save('a.png')
                x = -1 * (np.asarray(x, dtype=np.float) - 128) / 255
                x = x[:,:,np.newaxis]
                y = [np.float(i)]
                #print('x shape', x.shape, 'y', y)
                yield x, y
    dataset = tf.data.Dataset.from_generator(gen_data,
        output_types=(tf.float32, tf.float32),
        #output_shapes=((224, 224, 1), (10,))
        #output_shapes=(tf.TensorShape()(224, 224, 1), (10,))
    )
    if shuffle:
        dataset = dataset.shuffle(100)
    dataset = dataset.batch(batchsize)
    if repeat:
        dataset = dataset.repeat()
    iterator = dataset.make_initializable_iterator()
    return iterator


def plot_loss(model, history):
    plt.plot(history.history['loss'], 'o', label='Train')
    plt.plot(history.history['val_loss'], 'o', label='Validation')
    plt.title(model.name + ' Model Loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend()
    plot_fn = os.path.join('figures', '{0}_loss.png'.format(model.name))
    plt.savefig(plot_fn)
    plt.show()
    plt.close('all')


def main():
    sess = tf.Session()
    tf.keras.backend.set_session(sess)

    models = [
        (build_lenet_v1_model(IMG_SHAPE, NUM_CLASSES, '_1'), 5),
        #(build_lenet_v2_model(IMG_SHAPE, NUM_CLASSES, '_1'), 5),
        #(build_lenet_v1_model(IMG_SHAPE, NUM_CLASSES, '_2'), 30),
        #(build_lenet_v2_model(IMG_SHAPE, NUM_CLASSES, '_2'), 30),
        # build_test_model2(IMG_SHAPE, NUM_CLASSES),
    ]

    train_files = tf.placeholder(tf.string, shape=[None])
    test_files = tf.placeholder(tf.string, shape=[None])
    val_files = tf.placeholder(tf.string, shape=[None])

    dataset = {
        'train': image_input_fn(train_files,
                                shuffle=True,
                                batchsize=50,
                                repeat=True),
        'test': image_input_fn(test_files,
                               shuffle=False,
                               batchsize=50),
        'validation': image_input_fn(val_files,
                                     shuffle=True,
                                     batchsize=20),
    }
    sess.run(dataset['train'].initializer, feed_dict={
       train_files: ['tfrecords/train.tfrecord'],
    })
    sess.run(dataset['test'].initializer, feed_dict={
       test_files: ['tfrecords/test.tfrecord'],
    })
    sess.run(dataset['validation'].initializer, feed_dict={
       val_files: ['tfrecords/validation.tfrecord'],
    })
    # dataset = {
    #     'train': direct_input_fn('train',
    #                              shuffle=True,
    #                              batchsize=32,
    #                              repeat=True),
    #     'test': direct_input_fn('test',
    #                             shuffle=True,
    #                             batchsize=32),
    #     'validation': direct_input_fn('validation',
    #                                   shuffle=True,
    #                                   batchsize=32),
    # }
    # sess.run(dataset['train'].initializer)
    # sess.run(dataset['test'].initializer)
    # sess.run(dataset['validation'].initializer)
    sess.run(tf.global_variables_initializer())

    def train_and_test(model, steps_per_epoch):
        print('Training model', model.name)
        starttime = time()
        history = model.fit(
            x=dataset['train'],
            epochs=80,
            steps_per_epoch=steps_per_epoch,
            verbose=1,
            validation_data=dataset['validation'],
            validation_steps=1,
        )
        traintime = time() - starttime
        print('Train time', traintime, 'seconds')
        tests = []
        for i in range(1):
            score = model.evaluate(
                x=dataset['test'],
                steps=20,
                verbose=1,
            )
            test_data = {}
            for label, value in zip(model.metrics_names, score):
                test_data[label] = value
            tests.append(test_data)
        # Write test info
        df = pd.DataFrame(tests)
        print(tests)
        print('test acc', df['categorical_accuracy'])
        test_fn = os.path.join('data', '{0}_test.csv'.format(model.name))
        df.to_csv(test_fn)
        print('created', test_fn)
        # Plot loss
        plot_loss(model, history)
        # Write training history
        df = pd.DataFrame.from_dict(history.history)
        history_fn = os.path.join('data', '{0}_history.csv'.format(model.name))
        df.to_csv(history_fn)
        print('created', history_fn)
        # Write general info

        info_fn = os.path.join('data', '{0}_info.txt'.format(model.name))
        with open(info_fn, 'w') as f:
            avg_acc = np.mean(df['categorical_accuracy'])
            f.write('average test accuracy: {0}\n'.format(avg_acc))
            f.write('train time: {0} seconds\n'.format(traintime))
        print('created', info_fn)

    def show_model(model):
        model_fn = os.path.join('visual', '{0}.png'.format(model.name))
        tf.keras.utils.plot_model(model, show_shapes=True, to_file=model_fn)
        print('created', model_fn)
        graphviz_fn = os.path.join('visual', '{0}.gv'.format(model.name))
        ann_viz(model, view=True, filename=graphviz_fn, title=model.name)
        print('created', graphviz_fn)

    def show_layer_outputs(model, filename):
        img = Image.open(filename).resize(IMG_SHAPE[:2], Image.ANTIALIAS).convert('L')
        # img.show()
        outdir = os.path.join('visual', model.name)
        os.makedirs(outdir, exist_ok=True)
        img_fn = os.path.join(outdir, 'input.png')
        img.save(img_fn)
        img_arr = -1.0 * (np.array(img, dtype=np.float32) - 128) / 255.0
        # img_arr = (np.array(img, dtype=np.float32) - 127.0) / 255.0
        img_arr = img_arr[:, :, np.newaxis]
        tf.keras.backend.set_learning_phase(0)  # Set testing phase
        for i, layer in enumerate(model.layers, 1):
            base_cls = str(layer.__class__.__bases__[0])
            if 'Conv' not in base_cls:
                continue
            # print('layer', layer, i)
            layer_dir = os.path.join(outdir, '{0}_{1}'.format(i, layer.name))
            os.makedirs(layer_dir, exist_ok=True)
            get_layer = tf.keras.backend.function(inputs=[model.layers[0].input],
                                                  outputs=[layer.output])
            out_arr = get_layer([[img_arr]])[0]
            out_arr = np.array((-1.0 * out_arr) * 255 + 128, dtype=np.uint8)
            # print('arr size:', out_arr.shape, out_arr.dtype)
            channels = out_arr.shape[-1]
            # If there are 3 or less channels, represent each as a color in one image
            if channels <= 3:
                feature_map = out_arr[0, :, :]
                print('feature_map shape', feature_map)
                for j in range(3 - channels):
                    feature_map = np.append(feature_map,
                                            [np.zeros(feature_map.shape[:-1])], axis=0)
                fmap_img = Image.fromarray(feature_map, 'RGB')
                w, h = fmap_img.size
                feature_fn = os.path.join(layer_dir, '{0}x{1}.png'.format(w, h))
                fmap_img.save(feature_fn)
            else:
                for j in range(channels):
                    feature_map = out_arr[0, :, :, j]
                    # print('filter', j, 'shape:', feature_map.shape)
                    fmap_img = Image.fromarray(feature_map, 'L')
                    # print('output image size:', fmap_img.size)
                    # fmap_img.show()
                    w, h = fmap_img.size
                    feature_fn = os.path.join(layer_dir, '{0:0>3}_{1}x{2}.png'.format(j + 1, w, h))
                    fmap_img.save(feature_fn)

    def save_model(model):
        model_fn = os.path.join('model', '{0}.h5'.format(model.name))
        model.save(model_fn)

    def load_model(name):
        model_fn = os.path.join('model', '{0}.h5'.format(name))
        model = tf.keras.models.load_model(model_fn)
        return model

    def find_validation_losses(model):
        val_dataset = image_input_fn(val_files,
                                     shuffle=False,
                                     batchsize=1,
                                     repeat=False)
        sess.run(val_dataset.initializer, feed_dict={
            val_files: [
                #'tfrecords/train.tfrecord',
                'tfrecords/test.tfrecord',
                'tfrecords/validation.tfrecord',
            ],
        })
        losses = pd.DataFrame(columns=['filename', 'set', 'label', 'loss'])
        i = 0
        loss_index = model.metrics_names.index('loss')
        try:
            next = val_dataset.get_next()
            while True:
                x, y = sess.run(next)
                score = model.evaluate(x=x, y=y, verbose=0)
                # metrics = {label: value for label, value in zip(model.metrics_names, score)}
                name = x['name'].tostring().decode('utf-8').strip()
                label = str(tf.argmax(y, axis=1).eval()[0])
                loss = float(score[loss_index])
                if 'train' in name:
                    origin_set = 'train'
                elif 'test' in name:
                    origin_set = 'test'
                else:
                    origin_set = 'validation'
                losses = losses.append({'filename': name, 'set': origin_set, 'label': label, 'loss': loss},
                                       ignore_index = True)
                i += 1
        except tf.errors.OutOfRangeError:
            pass
        losses.sort_values(by=['label', 'loss'], ascending=False, inplace=True)
        losses = losses.reset_index(drop=True)
        losses_fn = os.path.join('data', '{0}_losses.csv'.format(model.name))
        losses.to_csv(losses_fn)
        print('created', losses_fn)

    with sess.as_default():
        for model, steps_per_epoch in models:
            train_and_test(model, steps_per_epoch)
            save_model(model)
            #model = load_model(model.name)
            show_layer_outputs(model, 'imu_mnist_2d_dataset/test/0/040.png')
            find_validation_losses(model)


if __name__ == '__main__':
    main()


