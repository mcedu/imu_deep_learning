import tensorflow as tf
from functools import reduce


def build_lenet_v1_model(input_shape, num_classes, subversion=''):
    #Original model
    model = tf.keras.models.Sequential(name="LeNet-5_v1" + subversion)
    model.add(tf.keras.layers.Conv2D(name='CONV1',
                                     input_shape=input_shape,
                                     filters=6,
                                     kernel_size=(5, 5),
                                     strides=(1, 1),
                                     padding='same',
                                     activation='tanh'))
    model.add(tf.keras.layers.AveragePooling2D(name='AVGPOOL1',
                                               pool_size=(2, 2),
                                               strides=(1, 1),
                                               padding='valid'))
    model.add(tf.keras.layers.Conv2D(name='CONV2',
                                     filters=16,
                                     kernel_size=(5, 5),
                                     strides=(1, 1),
                                     padding='valid',
                                     activation='tanh'))
    model.add(tf.keras.layers.AveragePooling2D(name='AVGPOOL2',
                                               pool_size=(2, 2),
                                               strides=(2, 2),
                                               padding='valid'))
    model.add(tf.keras.layers.Conv2D(name='CONV3',
                                     filters=120,
                                     kernel_size=(5, 5),
                                     strides=(1, 1),
                                     padding='valid',
                                     activation='tanh'))
    model.add(tf.keras.layers.Flatten(name='FC1'))
    model.add(tf.keras.layers.Dense(name='FC2', units=84, activation='tanh'))
    model.add(tf.keras.layers.Dense(name='FC3', units=num_classes,
                                    activation='softmax'))
    model.compile(loss=tf.keras.losses.categorical_crossentropy,
                  optimizer='SGD',
                  metrics=['categorical_accuracy', 'mae', 'mse'])
    return model


def build_lenet_v2_model(input_shape, num_classes, subversion=''):
    #Wide model
    model = tf.keras.models.Sequential(name="LeNet-5_v2" + subversion)
    model.add(tf.keras.layers.Conv2D(name='CONV1',
                                     input_shape=input_shape,
                                     filters=6,
                                     kernel_size=(3, 3),
                                     strides=(1, 1),
                                     padding='same',
                                     activation='tanh'))
    model.add(tf.keras.layers.AveragePooling2D(name='AVGPOOL1',
                                               pool_size=(2, 2),
                                               strides=(1, 1),
                                               padding='valid'))
    model.add(tf.keras.layers.Conv2D(name='CONV2',
                                     filters=16,
                                     kernel_size=(3, 3),
                                     padding='valid',
                                     activation='tanh'))
    model.add(tf.keras.layers.AveragePooling2D(name='AVGPOOL2',
                                               pool_size=(2, 2),
                                               padding='valid'))
    model.add(tf.keras.layers.Conv2D(name='CONV3',
                                     filters=120,
                                     kernel_size=(3, 3),
                                     strides=(1, 1),
                                     padding='valid',
                                     activation='tanh'))
    model.add(tf.keras.layers.Flatten(name='FC1'))
    model.add(tf.keras.layers.Dense(name='FC2', units=1000, activation='tanh'))
    model.add(tf.keras.layers.Dense(name='FC3', units=num_classes,
                                    activation='softmax'))
    model.compile(loss=tf.keras.losses.categorical_crossentropy,
                  optimizer='SGD',
                  metrics=['categorical_accuracy', 'mae', 'mse'])
    return model


def build_test_model(input_shape, num_classes):
    model = tf.keras.models.Sequential([
        tf.keras.layers.Flatten(input_shape=input_shape),
        tf.keras.layers.Dense(reduce(lambda a, x: a * x, input_shape), activation=tf.nn.relu),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(num_classes, activation=tf.nn.softmax)
    ], name='TEST')
    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['categorical_accuracy'])
    return model


def build_test_model2(input_shape, num_classes):
    model = tf.keras.models.Sequential(name='TEST2')
    model.add(tf.keras.layers.Conv2D(
          input_shape=input_shape,
          name='CONV1',
          filters=32,
          kernel_size=[5, 5],
          padding="same",
          activation=tf.nn.relu))

    # Pooling Layer #1
    model.add(tf.keras.layers.MaxPooling2D(pool_size=[2, 2], strides=2))

    # Convolutional Layer #2 and Pooling Layer #2
    model.add(tf.keras.layers.Conv2D(
      filters=64,
      kernel_size=[5, 5],
      padding="same",
      activation=tf.nn.relu))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=[2, 2], strides=2))
    print('model size', model.output.shape)

    # Dense Layer
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dropout(rate=0.25))
    model.add(tf.keras.layers.Dense(units=1000, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dropout(rate=0.25))

    # Logits Layer
    model.add(tf.keras.layers.Dense(units=num_classes, activation='softmax'))
    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['categorical_accuracy'])
    return model

