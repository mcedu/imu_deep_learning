import numpy as np
import os
import pandas as pd
import re
import tensorflow as tf
import matplotlib.pyplot as plt

from PIL import Image


datadir = 'imu_mnist_2d_dataset/'

#groups = ['train', 'test', 'validation']
groups = ['validation']

column_names = [
    'ETime', 'Eh', 'Er', 'Ep',
    'Qx', 'Qy', 'Qz', 'Qw',
    'Gx', 'Gy', 'Gz',
    'Ax', 'Ay', 'Az',
    'LAx', 'LAy', 'LAz',
    'Mx', 'My', 'Mz',
    'Vx', 'Vy', 'Vz',
    'Px', 'Py', 'Pz',
]


def generate_images(imu_fn, label, group, group_dir):
    print(imu_fn)
    _, basename = os.path.split(imu_fn)
    basename, _ = os.path.splitext(basename)
    df = pd.read_csv(imu_fn, skiprows=1, delimiter='\s+', usecols=column_names)
    # Get LA data
    pos = np.array(df[['Px', 'Py']], dtype=np.float32)
    # Save images
    #fig, ((axes['XY'], axes['YZ']), (axes['XZ'], _)) = plt.subplots(nrows=2, ncols=2)
    #_.axis('off')
    x_axis = pos[:,0]
    y_axis = pos[:,1]
    fig = plt.figure()
    ax = fig.gca()
    #ax = axes[plane]
    #ax.set_title(plane)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.axis('off')
    ax.plot(x_axis, y_axis, linewidth=10, color='black')
    #max_range = np.array([LAx.max() - LAx.min(), LAy.max() - LAy.min(), LAz.max() - LAz.min()]).max()
    #Xb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][0].flatten() + 0.5 * (LAx.max() + LAx.min())
    #Yb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][1].flatten() + 0.5 * (LAy.max() + LAy.min())
    #Zb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][2].flatten() + 0.5 * (LAz.max() + LAz.min())
    #for xb, yb, zb in zip(Xb, Yb, Zb):
    #    ax.plot([xb], [yb], [zb], 'w')
    img_fn = os.path.join(group_dir, '{0}.png'.format(basename))
    fig.savefig(img_fn)
    #plt.show()
    plt.close('all')


def main():
    for group in groups:
        for label in range(10):
            cls = str(label)
            group_dir = os.path.join(datadir, group, cls)
            if not os.path.exists(group_dir):
                continue
            count = 0
            for fn in sorted(os.listdir(group_dir)):
                if not fn.endswith('.imu'):
                    continue
                #if count + 1 > 100:
                #    break
                count += 1
                generate_images(
                    imu_fn=os.path.join(group_dir, fn),
                    label=label,
                    group=group,
                    group_dir=group_dir,
                )


if __name__ == '__main__':
    main()

